﻿/* Copyright (c) 2015, pg-scoreboard
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
using PG_Scoreboard.BlackboardImpl;
using PG_Scoreboard.Interfaces;
using PG_Scoreboard.Interfaces.IBlackboardParameters.Base;
using scoreboard.aircraftAnalyzer.Constants;
using scoreboard.aircraftAnalyzer.Experts;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace scoreboard.aircraftAnalyzer.Tests
{
    public class TestExpert : BaseProcessExpert
    {
        public SimpleBlackboardEntryType inType { get; set; }
        public SimpleBlackboardEntryType outType { get; set; }
        public string phase { get; set; }
        public double successRate { get; set; }
        public double timeMod { get; set; }

        private string name;

        private Random random = new Random();


        public TestExpert(IBlackboard blk, SimpleBlackboardEntryType inType, SimpleBlackboardEntryType outType, string name, string phase, double successRate, double timeMod)
			: base(blk)
		{
            Debug.Assert(outType != null);

            this.inType = inType;
            this.outType = outType;
            this.phase = phase;
            this.successRate = successRate;
            this.timeMod = timeMod;
            this.name = name;
		}
		
		public override void DoWork()
		{
            if (inType == null)
            {
                blackboard.AddEntry(new BlackboardEntry(new TestResult()
                {
                    result = "GOOD",
                    basedOn = null
                }, this.GetName(), EntryTypes.RADAR_SIGNATURE, 1));

                return;
            }

            IEnumerable<BlackboardEntry> collect = blackboard.GetEntries(inType).Except(processed);

            //int time = (int)((50 * collect.Count()) * (((random.NextDouble() + 0.0000001) / 2) + 0.5));

            //Thread.Sleep(time);

            if (inType == EntryTypes.RADAR_SIGNATURE)
            {
                string result = random.NextDouble() < successRate ? "GOOD" : "BAD";

                blackboard.AddEntry(new BlackboardEntry(new TestResult()
                {
                    result = result,
                    basedOn = collect.First().data as TestResult
                }, this.GetName(), EntryTypes.GUESS_PARTS, this.successRate));

                processed.Add(collect.First());
            }
            else
            if (inType == EntryTypes.GUESS_PARTS)
            {
                var test = collect.ToList();
                var res = collect.GroupBy(x => (x.data as TestResult).basedOn).ToList().FirstOrDefault();
                List<TestResult> results = new List<TestResult>(res.Take(4).Select(x => x.data as TestResult).ToList());

                if (results.Count == 0)
                    return;

                double propability = 3 - results.Count();

                foreach (TestResult tr in results)
                {
                    if (tr.result == "GOOD")
                    {
                        propability += 0.5;
                    }
                    else
                    {
                        propability -= 0.5;
                    }
                }

                propability /= 3;

                string result = random.NextDouble() < successRate + propability ? "GOOD" : "BAD";

                blackboard.AddEntry(new BlackboardEntry(new TestResult()
                {
                    result = result,
                    basedOn = res.First().data as TestResult
                }, this.GetName(), EntryTypes.RESULTS, this.successRate));

                res.Take(4).ToList().ForEach(x => processed.Add(x));
            }
		}

        public override string GetName()
        {
            return name;
        }

        int count = 5;

        public override int InitCondition(List<BlackboardEntry> entries)
        {
            if (inType != null) 
            {
                return entries.Except(processed).Any(entry => entry.Type == inType) == true ? 0 : -1;
            }
            else
            {
                count -= 1;
                return count > 0 ? 0 : 1;
            }
        }

        public override bool processesPhase(string phase)
        {
            return phase.CompareTo(this.phase) == 0;
        }
    }
}
