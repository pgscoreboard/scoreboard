﻿/* Copyright (c) 2015, pg-scoreboard
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
using PG_Scoreboard.BlackboardImpl;
using PG_Scoreboard.Interfaces;
using PG_Scoreboard.Interfaces.IBlackboardParameters;
using PG_Scoreboard.Interfaces.IBlackboardParameters.Base;
using scoreboard.aircraftAnalyzer.Constants;
using scoreboard.aircraftAnalyzer.datasource;
using scoreboard.aircraftAnalyzer.Datasource;
using scoreboard.aircraftAnalyzer.Models;
using scoreboard.aircraftAnalyzer.Models.Engines;
using scoreboard.aircraftAnalyzer.Models.Hulls;
using scoreboard.aircraftAnalyzer.Models.interfaces;
using scoreboard.aircraftAnalyzer.Models.Interfaces;
using scoreboard.aircraftAnalyzer.Models.Wings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace scoreboard.aircraftAnalyzer.Experts
{
    class AircraftGuessExpert : BaseProcessExpert
    {
        public AircraftGuessExpert(IBlackboard blk)
			: base(blk)
		{

		}

        private IDictionary<IHull, int> hulls = new Dictionary<IHull, int>();
        private IDictionary<IEngine, int> engines = new Dictionary<IEngine, int>();
        private IDictionary<IWing, int> wings = new Dictionary<IWing, int>();

		public override void DoWork()
		{
            IEnumerable<BlackboardEntry> collect = blackboard.GetEntries(EntryTypes.GUESS_PARTS).Except(processed);

            IGuessParts guessed = (collect.First().data as IGuessParts);
            ISignature guessedUpon = guessed.GuessedUpon.First();

            IEnumerable<BlackboardEntry> forSig = collect.Where(
                x => (x.data as IGuessParts).GuessedUpon.First() == guessedUpon);

            hulls.Clear();
            engines.Clear();
            wings.Clear();

            foreach (BlackboardEntry entry in forSig)
            {
                IGuessParts guess = entry.data as IGuessParts;

                if (guess.Engine != null)
                    addOrIncrement(engines, guess.Engine);

                if (guess.Hull != null)
                    addOrIncrement(hulls, guess.Hull);

                if (guess.Wing != null)
                    addOrIncrement(wings, guess.Wing);

                processed.Add(entry);
            }

            IHull hull = hulls.Count() > 0 ? hulls.FirstOrDefault(x => x.Value == hulls.Values.Max()).Key : null;
            IEngine engine = engines.Count() > 0 ? engines.FirstOrDefault(x => x.Value == engines.Values.Max()).Key : null;
            IWing wing = wings.Count() > 0 ? wings.FirstOrDefault(x => x.Value == wings.Values.Max()).Key : null;

            double assurance = 0.0;

            foreach (IAircraft craft in resolveAircraft(hull, engine, wing, out assurance))
            {
                IGuessAircraft aircraft = new GuessAircraft()
                {
                    aircraft = craft,
                    guessedUponSig = guessedUpon
                };

                blackboard.AddEntry(new BlackboardEntry(aircraft, this.GetName(), EntryTypes.RESULTS, assurance));
            }
        }

        private void addOrIncrement<T>(IDictionary<T, int> dict, T key)
        {
            if (dict.ContainsKey(key))
                dict[key] = dict[key] + 1;
            else
                dict.Add(key, 1);
        }

        private IEnumerable<IAircraft> resolveAircraft(IHull hull, IEngine engine, IWing wing, out double assurance)
        {
            if (hull == null)
                hull = new UnknownHull();

            if (engine == null)
                engine = new UnknownEngine();

            if (wing == null)
                wing = new UnknownWing();

            IList<Func<IAircraft, bool>> predicates = new List<Func<IAircraft, bool>>()
            {
                x => x.Engine == engine && x.Hull == hull && x.Wing == wing,
                x => x.Engine == engine && x.Hull == hull,
                x => x.Engine == engine && x.Wing == wing,
                x => x.Wing == wing && x.Hull == hull,
                x => x.Engine == engine,
                x => x.Hull == hull,
                x => x.Wing == wing
            };

            IEnumerable<IAircraft> resulting = null;

            foreach (Func<IAircraft, bool> pred in predicates)
            {
                resulting = KnownCollections.OrderOfCrafts.Where(pred);

                if (resulting.Count() > 0)
                    break;
            }

            if (resulting.Count() > 0)
                assurance = 1.0 / resulting.Count();
            else
                assurance = 0;

            return resulting;
        }
        
        public override int InitCondition(List<BlackboardEntry> entries)
        {
            return entries.Except(processed).Any(entry => entry.Type == EntryTypes.GUESS_PARTS) == true ? 0 : -1;
        }

        public override bool processesPhase(string phase)
        {
            return phase.CompareTo(Phases.GUESS_PHASE) == 0;
        }
    }
}
