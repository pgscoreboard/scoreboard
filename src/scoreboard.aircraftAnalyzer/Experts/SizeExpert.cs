﻿/* Copyright (c) 2015, pg-scoreboard
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
using PG_Scoreboard.BlackboardImpl;
using PG_Scoreboard.Interfaces;
using PG_Scoreboard.Interfaces.Base;
using PG_Scoreboard.Interfaces.IBlackboardParameters;
using PG_Scoreboard.Interfaces.IBlackboardParameters.Base;
using scoreboard.aircraftAnalyzer.Constants;
using scoreboard.aircraftAnalyzer.datasource;
using scoreboard.aircraftAnalyzer.Datasource;
using scoreboard.aircraftAnalyzer.Models;
using scoreboard.aircraftAnalyzer.Models.interfaces;
using scoreboard.aircraftAnalyzer.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace scoreboard.aircraftAnalyzer.Experts
{
    class SizeExpert : BaseProcessExpert
    {
        public SizeExpert(IBlackboard blk)
			: base(blk)
		{

		}

        public override void DoWork()
		{
            IEnumerable<BlackboardEntry> collect = blackboard.GetEntries(EntryTypes.RADAR_SIGNATURE).Except(processed);

            foreach (BlackboardEntry entry in collect)
            {
                ISignature sig = entry.data as ISignature;

                if (Radar.DISTANCE_TO_SIZES > sig.Distance && sig.HullSize == null)
                {
                    DoWorkForClose(sig);
                }
                else
                    if (Radar.DISTANCE_TO_TOTAL_SIZE > sig.Distance && sig.HullSize != null)
                {
                    DoWorkForFar(sig);
                }

                processed.Add(entry);
            }
		}

        private readonly int SIZE_DIFFERENCE_PARAMETER = 4000;

        private void DoWorkForFar(ISignature sig)
        {
            /* Only consider width, it's the best heuristics from this distance */
            IEnumerable<IAircraft> possibleCrafts = KnownCollections.OrderOfCrafts.Where(x =>
                x.Hull.HullSize.Width + x.Wing.WingSize.Width < sig.Size.Width + SIZE_DIFFERENCE_PARAMETER &&
                x.Hull.HullSize.Width + x.Wing.WingSize.Width > sig.Size.Width - SIZE_DIFFERENCE_PARAMETER);

            foreach (IAircraft craft in possibleCrafts)
            {
                double assurance = calculateAssurance(possibleCrafts.Count(),
                    null, null, null, null);

                BlackboardEntry toAdd = new BlackboardEntry(new GuessParts()
                {
                    GuessedUpon = new List<ISignature>() { sig },
                    Hull = craft.Hull,
                    Engine = craft.Engine
                }, this.GetName(), EntryTypes.GUESS_PARTS, assurance);

                this.blackboard.AddEntry(toAdd);
            }
        }

        private readonly int SIZE_DIFFERENCE_CLOSE_PARAMETER = 500;

        private void DoWorkForClose(ISignature sig)
        {
            /* a bit ugly, but can't help it i guess */
            IEnumerable<IHull> possibleHulls = KnownCollections.OrderOfHulls.Where(x => 
                (x.HullSize.Width < sig.HullSize.Width + SIZE_DIFFERENCE_CLOSE_PARAMETER &&
                x.HullSize.Width > sig.HullSize.Width - SIZE_DIFFERENCE_CLOSE_PARAMETER) ||
                (x.HullSize.Height < sig.HullSize.Height + SIZE_DIFFERENCE_CLOSE_PARAMETER &&
                x.HullSize.Height > sig.HullSize.Height - SIZE_DIFFERENCE_CLOSE_PARAMETER) ||
                (x.HullSize.Cubature < sig.HullSize.Cubature + SIZE_DIFFERENCE_CLOSE_PARAMETER &&
                x.HullSize.Cubature > sig.HullSize.Cubature - SIZE_DIFFERENCE_CLOSE_PARAMETER));
            
            IEnumerable<IWing> possibleWings = KnownCollections.OrderOfWings.Where(x => 
                (x.WingSize.Width < sig.WingSize.Width + SIZE_DIFFERENCE_CLOSE_PARAMETER &&
                x.WingSize.Width > sig.WingSize.Width - SIZE_DIFFERENCE_CLOSE_PARAMETER) ||
                (x.WingSize.Height < sig.WingSize.Height + SIZE_DIFFERENCE_CLOSE_PARAMETER &&
                x.WingSize.Height > sig.WingSize.Height - SIZE_DIFFERENCE_CLOSE_PARAMETER) ||
                (x.WingSize.Cubature < sig.WingSize.Cubature + SIZE_DIFFERENCE_CLOSE_PARAMETER &&
                x.WingSize.Cubature > sig.WingSize.Cubature - SIZE_DIFFERENCE_CLOSE_PARAMETER));

            foreach (IHull hull in possibleHulls)
            {
                double assurance = calculateAssurance(possibleHulls.Count(),
                    null, null, null, null);

                BlackboardEntry toAdd = new BlackboardEntry(new GuessParts()
                {
                    GuessedUpon = new List<ISignature>() { sig },
                    Hull = hull
                }, this.GetName(), EntryTypes.GUESS_PARTS, assurance);

                this.blackboard.AddEntry(toAdd);
            }

            foreach (IWing wing in possibleWings)
            {
                double assurance = calculateAssurance(possibleWings.Count(),
                    null, null, null, null);

                BlackboardEntry toAdd = new BlackboardEntry(new GuessParts()
                {
                    GuessedUpon = new List<ISignature>() { sig },
                    Wing = wing
                }, this.GetName(), EntryTypes.GUESS_PARTS, assurance);

                this.blackboard.AddEntry(toAdd);
            }
        }
        
        public override int InitCondition(List<BlackboardEntry> entries)
        {
            return entries.Except(processed).Any(entry => entry.Type == EntryTypes.RADAR_SIGNATURE) == true ? 0 : -1;
        }

        public override bool processesPhase(string phase)
        {
            return phase.CompareTo(Phases.PROCESS_PHASE) == 0;
        }
    }
}
