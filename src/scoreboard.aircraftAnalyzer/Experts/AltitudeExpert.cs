﻿/* Copyright (c) 2015, pg-scoreboard
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
using PG_Scoreboard.BlackboardImpl;
using PG_Scoreboard.Interfaces;
using PG_Scoreboard.Interfaces.Base;
using PG_Scoreboard.Interfaces.IBlackboardParameters;
using PG_Scoreboard.Interfaces.IBlackboardParameters.Base;
using PG_Scoreboard.Interfaces.IExpertParameters.Base;
using scoreboard.aircraftAnalyzer.Constants;
using scoreboard.aircraftAnalyzer.datasource;
using scoreboard.aircraftAnalyzer.Datasource;
using scoreboard.aircraftAnalyzer.Models.interfaces;
using scoreboard.aircraftAnalyzer.Models.Interfaces;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace scoreboard.aircraftAnalyzer.Experts
{
    class AltitudeExpert : BaseProcessExpert
    {
        public AltitudeExpert(IBlackboard blk)
			: base(blk)
		{

		}

        public override void DoWork()
		{
            IEnumerable<BlackboardEntry> collect = blackboard.GetEntries(EntryTypes.RADAR_SIGNATURE).Except(processed);

            foreach (BlackboardEntry entry in collect)
            {
                ISignature sig = entry.data as ISignature;

                if (Radar.DISTANCE_TO_SPEED_ALTITUDE > sig.Distance)
                {
                    IEnumerable<IEngine> possibleEngines = KnownCollections.OrderOfEngines.Where(x => 
                        x.MaxAltitude.AboveSea > sig.Altitude.AboveSea &&
                        x.MinAltitude.AboveSea < sig.Altitude.AboveSea);

                    foreach (IEngine engine in possibleEngines)
                    {
                        double assurance = calculateAssurance(possibleEngines.Count(), 
                            null, engine.MaxAltitude.AboveSea, engine.MinAltitude.AboveSea, sig.Altitude.AboveSea);

                        BlackboardEntry toAdd = new BlackboardEntry(new GuessParts()
                        {
                            GuessedUpon = new List<ISignature>() { sig },
                            Engine = engine
                        }, this.GetName(), EntryTypes.GUESS_PARTS, assurance);

                        this.blackboard.AddEntry(toAdd);
                    }
                }

                processed.Add(entry);
            }
		}
        
        public override int InitCondition(List<BlackboardEntry> entries)
        {
            return entries.Except(processed).Any(entry => entry.Type == EntryTypes.RADAR_SIGNATURE) == true ? 0 : -1;
        }

        public override bool processesPhase(string phase)
        {
            return phase == Phases.PROCESS_PHASE_T;
        }
    }
}
