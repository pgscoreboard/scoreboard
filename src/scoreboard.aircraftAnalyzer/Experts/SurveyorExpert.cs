﻿/* Copyright (c) 2015, pg-scoreboard
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
using PG_Scoreboard.BlackboardImpl;
using PG_Scoreboard.Interfaces;
using PG_Scoreboard.Interfaces.Base;
using PG_Scoreboard.Interfaces.IBlackboardParameters;
using PG_Scoreboard.Interfaces.IBlackboardParameters.Base;
using PG_Scoreboard.Interfaces.IExpertParameters.Base;
using PG_Scoreboard.Utils;
using scoreboard.aircraftAnalyzer.Constants;
using scoreboard.aircraftAnalyzer.datasource;
using scoreboard.aircraftAnalyzer.Models.Aircraft;
using scoreboard.aircraftAnalyzer.Models.Impl;
using scoreboard.aircraftAnalyzer.Models.interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace scoreboard.aircraftAnalyzer.Experts
{
    class SurveyorExpert : LoggedExpert
    {
        private Logger l = new Logger(typeof(SurveyorExpert));
        private Radar radar;

        private bool disabled;

        public SurveyorExpert(IBlackboard blackboard, Radar radar)
            : base(blackboard)
        {
            this.radar = radar;
            this.blackboard = blackboard;

            this.condition = new BaseWorkCondition(x => disabled == false ? 0 : -1);

            radar.BuildScenario(
                new Radar.ScenarioBuilder()
                    .withPhase(new Radar.ScenarioBuilder.Phase()
                        .AddCraft(Airliner.New(), new Radar.AircraftState()
                            .withDistance(1000)
                            .withSpeedX(0)
                            .withSpeedY(-700)
                            .withAltitude(17000))
                        .AddCraft(Airliner.New(), new Radar.AircraftState()
                            .withDistance(1000)
                            .withSpeedX(0)
                            .withSpeedY(-650)
                            .withAltitude(23000))
                        .AddCraft(Airliner.New(), new Radar.AircraftState()
                            .withDistance(1000)
                            .withSpeedX(0)
                            .withSpeedY(-650)
                            .withAltitude(23000))
                        .AddCraft(Airliner.New(), new Radar.AircraftState()
                            .withDistance(1000)
                            .withSpeedX(0)
                            .withSpeedY(-650)
                            .withAltitude(23000)))
                    .withPhase(new Radar.ScenarioBuilder.Phase())
                    .withPhase(new Radar.ScenarioBuilder.Phase()
                        .AddCraft(Jet.New(), new Radar.AircraftState()
                            .withDistance(1000)
                            .withSpeedX(0)
                            .withSpeedY(-780)
                            .withAltitude(17000)))
                    .withPhase(new Radar.ScenarioBuilder.Phase())
                    .withPhase(new Radar.ScenarioBuilder.Phase())
                    .withPhase(new Radar.ScenarioBuilder.Phase())
                    .withPhase(new Radar.ScenarioBuilder.Phase())
                    .Build()
            );
        }

        List<ISignature> alreadyBroadcasted = new List<ISignature>();

        public override void DoWork()
        {
            if (disabled)
                return;

            IEnumerable<ISignature> sigs = radar.Search();

            var arr = sigs.Count();

            foreach (ISignature signature in sigs)
            {
                this.blackboard.AddEntry(new BlackboardEntry(signature, this.GetName(), EntryTypes.RADAR_SIGNATURE, 1.0));

                if (alreadyBroadcasted.Where(x => x.uid == signature.uid).Count() == 0)
                {
                    l.log.Debug(String.Format("-- RADAR EVENT ---- New aircraft was spotted: distance <{0}> altitude <{1}> __its actually a <<{2}>>",
                        signature.Distance,
                        signature.Altitude.AboveSea,
                        (signature as Signature).__Hidden_aircraft.GetType().Name));

                    alreadyBroadcasted.Add(signature);
                }
            }

            disabled = radar.passPhase();
        }

        public override bool processesPhase(string phase)
        {
            if (disabled)
                return false;

            if (phase.CompareTo(Phases.RADAR_PHASE) == 0)
            {
                return true;
            }

            return false;
        }

        public override string GetName()
        {
            return this.GetType().Name;
        }
    }
}
