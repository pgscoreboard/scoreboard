﻿/* Copyright (c) 2015, pg-scoreboard
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
using scoreboard.aircraftAnalyzer.Models;
using scoreboard.aircraftAnalyzer.Models.Impl;
using scoreboard.aircraftAnalyzer.Models.interfaces;
using scoreboard.aircraftAnalyzer.Models.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace scoreboard.aircraftAnalyzer.datasource
{
    public class Radar
    {
    	private IList<ScenarioBuilder.Phase> phases;
    	private IEnumerator<ScenarioBuilder.Phase> currentPhase;

        public static readonly uint DISTANCE_TO_EXISTENCE = 4000;
        public static readonly uint DISTANCE_TO_SPEED_ALTITUDE = 3000;
        public static readonly uint DISTANCE_TO_TOTAL_SIZE = 2000;
        public static readonly uint DISTANCE_TO_SIZES = 1000;

        public IEnumerable<ISignature> Search()
        {
        	ScenarioBuilder.Phase phase = currentPhase.Current;

        	IEnumerable<ISignature> signatures = phase.getCraftData().Select(x => processIntoSignature(x));

            return signatures;
        }
        
        private ISignature processIntoSignature(ScenarioBuilder.AircraftData data)
        {
            if (data.state.Distance <= DISTANCE_TO_EXISTENCE)
            {
                ISignature sig = new Signature()
                {
                    uid = data.id,
                    Distance = data.state.Distance,
                    Altitude = ParseAltitude(data),
                    Size = ParseSize(data),
                    Speed = ParseSpeed(data),
                    WingSize = ParseWingSize(data),
                    HullSize = ParseHullSize(data),
                    __Hidden_aircraft = data.aircraft
                };

                return sig;
            }

            return null;
        }

        private Altitude ParseAltitude(ScenarioBuilder.AircraftData data)
        {
            if (data.state.Distance < DISTANCE_TO_SPEED_ALTITUDE)
            {
                return new Altitude(data.state.Altitude);
            }

            return null;
        }

        private Size ParseSize(ScenarioBuilder.AircraftData data)
        {
            if (data.state.Distance < DISTANCE_TO_TOTAL_SIZE)
            {
                return data.aircraft.GetTotalSize();
            }

            return null;
        }

        private Speed ParseSpeed(ScenarioBuilder.AircraftData data)
        {
            if (data.state.Distance < DISTANCE_TO_SPEED_ALTITUDE)
            {
                return data.state.getSpeed();
            }

            return null;
        }

        private Size ParseWingSize(ScenarioBuilder.AircraftData data)
        {
            if (data.state.Distance < DISTANCE_TO_SIZES)
            {
                return data.aircraft.Wing.WingSize;
            }

            return null;
        }

        private Size ParseHullSize(ScenarioBuilder.AircraftData data)
        {
            if (data.state.Distance < DISTANCE_TO_SIZES)
            {
                return data.aircraft.Hull.HullSize;
            }

            return null;
        }
        
        public bool passPhase()
        {
            return !currentPhase.MoveNext();
        }

        /// <summary>
        /// builds sequence of phases (that happen upon X turns passing), that might modify our knowledgebase
        /// by adding new Aircrafts
        /// </summary>
        /// <param name="phases">list of phases, can be constructed by ScenarioBuilder</param>
        /// <param name="stepInTurns">after how much turns next phase should start</param>
        public void BuildScenario(List<ScenarioBuilder.Phase> phases)
        {
			this.phases = phases;
			this.currentPhase = phases.GetEnumerator();

            this.currentPhase.MoveNext();
        }

        public class AircraftState
        {
            public int Distance { get; set; }
            public int SpeedX { get; set; }
            public int SpeedY { get; set; }

            public int Altitude { get; set; }

            public AircraftState withDistance(int Distance) { this.Distance = Distance; return this; }
            public AircraftState withSpeedX(int SpeedX) { this.SpeedX = SpeedX; return this; }
            public AircraftState withSpeedY(int SpeedY) { this.SpeedY = SpeedY; return this; }
            public AircraftState withAltitude(int Altitude) { this.Altitude = Altitude; return this; }

            public Speed getSpeed()
            {
                return new Speed(SpeedX, SpeedY);
            }
        }

        public class ScenarioBuilder
        {
            public ScenarioBuilder()
            {
                phases = new List<Phase>();
            }
            
            public class AircraftData
            {
                public Guid id { get; set; }
            	public IAircraft aircraft { get; set; }
            	public AircraftState state  { get; set; }
            }

            public class Phase
            {
            	private IList<AircraftData> data = new List<AircraftData>();
            	
                public Phase AddCraft(IAircraft craft, AircraftState state)
                {
                	data.Add(new AircraftData()
                	         {
                                 id = Guid.NewGuid(),
                	         	 aircraft = craft,
                	         	 state = state
                	         });
                	
                    return this;
                }

                public Phase copyIntoAndAct(Phase previous)
                {
                    IList<AircraftData> conted = previous.data
                            .ToList()
                            .Select(x => new AircraftData() 
                            { 
                                aircraft = x.aircraft,
                                id = x.id,
                                state = new AircraftState() 
                                { 
                                    Altitude = x.state.Altitude,
                                    Distance = x.state.Distance + x.state.SpeedY,
                                    SpeedX = x.state.SpeedX,
                                    SpeedY = x.state.SpeedY
                                }
                            }).Concat(this.data).ToList();

                    this.data = conted;

                    return this;
                }
                
                public IEnumerable<AircraftData> getCraftData()
                {
                	foreach (AircraftData a in data)
                		yield return a;
                }
            }

            public ScenarioBuilder withPhase(Phase phase)
            {
                if (phases.Count > 0)
                {
                    phase.copyIntoAndAct(phases.Last());
                }

                phases.Add(phase);
                
                return this;
            }

            public List<Phase> Build()
            {
                return phases;
            }

            private List<Phase> phases;
        }
    }
}
