﻿/* Copyright (c) 2015, pg-scoreboard
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
using PG_Scoreboard.BlackboardImpl;
using PG_Scoreboard.Interfaces;
using PG_Scoreboard.Interfaces.IBlackboardParameters;
using PG_Scoreboard.Interfaces.IExpertParameters;
using PG_Scoreboard.Interfaces.IExpertParameters.Base;
using PG_Scoreboard.ScoreboardImpl;
using PG_Scoreboard.ScoreboardImpl.Moderator;
using PG_Scoreboard.Utils;
using scoreboard.aircraftAnalyzer.Constants;
using scoreboard.aircraftAnalyzer.Datasource;
using scoreboard.aircraftAnalyzer.Models;
using scoreboard.aircraftAnalyzer.Models.Aircraft;
using scoreboard.aircraftAnalyzer.Models.Aircrafts;
using scoreboard.aircraftAnalyzer.Models.Engines;
using scoreboard.aircraftAnalyzer.Models.Hulls;
using scoreboard.aircraftAnalyzer.Models.Impl;
using scoreboard.aircraftAnalyzer.Models.interfaces;
using scoreboard.aircraftAnalyzer.Models.Interfaces;
using scoreboard.aircraftAnalyzer.Models.Wings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace scoreboard.aircraftAnalyzer.SpecialExperts
{
    public class Moderator : BaseModerator
    {
        private Logger l = new Logger(typeof(Moderator));

        private IList<BlackboardEntry> processed = new List<BlackboardEntry>();
        
        public class ExpertData
        {
            public int Passed { get; set; }
            public int Failed { get; set; }

            public ExpertData()
            {
                Passed = 0;
                Failed = 0;
            }
        }

        public IDictionary<string, ExpertData> Results { get; set; }

        //Add this rather than ((ScoreboardController)controller).RunModeratorEachTurn = true;
	    override public bool processesPhase(string phase)
        {
            return true;
        }

        public int InitCondition(IList<BlackboardEntry> entries)
        {
            
            if (entries.Except(processed).Count() > 0)
            {
                return 0;
            }

            return -1;
        }

        public Moderator(IBlackboard blk, IScoreboard<ScoreboardEntry> scr)
            : base(blk, scr)
		{
            condition = new BaseWorkCondition(x => InitCondition(x));
            Results = new Dictionary<string, ExpertData>();
		}

        public override void DoWork()
		{
            IEnumerable<BlackboardEntry> entries = blackboard.GetEntries().Except(processed);

            foreach (BlackboardEntry entry in entries)
            {
                double score = -1.0;

                if (entry.Type == EntryTypes.GUESS_PARTS)
                {
                    score = ProcessProcess(entry);
                    processed.Add(entry);

                    addOrModifyScore(entry.ModuleName, score, 1);
                }
                else
                    if (entry.Type == EntryTypes.RESULTS)
                    {
                        score = ProcessGuess(entry);
                        processed.Add(entry);

                        addOrModifyScore(entry.ModuleName, score, 1);
                    }
            }
		}

        private void addOrModifyScore(string who, double score, int callTime)
        {
            ScoreboardEntry entry = Scoreboard.GetEntries().FirstOrDefault(x => x.ScoredName == who);

            if (entry == null) 
            {
                Scoreboard.AddEntry(new ScoreboardEntry(who, score, 1));
            }
            else
            {
                Scoreboard.AddEntry(new ScoreboardEntry(who, (entry.Score + score) / 2, 1));
            }
        }

        /// <summary>
        /// Calculates score for a given entry
        /// Rules:
        ///     If GuessedUpon signatures are not of the same craft, expert mixed them, and fails instantly
        ///     if any part has score of -1, it's not considered
        ///     if nothing interesting was found, get average score (0.5), 
        /// repeated failure of producing results is not considered
        /// </summary>
        /// <param name="entry">entry that is currently processed</param>
        /// <returns>
        /// Score given for this entry
        /// </returns>
        private double ProcessProcess(BlackboardEntry entry)
        {
            IGuessParts parts = entry.data as IGuessParts;

            Signature sig = (Signature)parts.GuessedUpon.First();

            if (!(parts.GuessedUpon.All(x => ((Signature)x).__Hidden_aircraft.Engine == sig.__Hidden_aircraft.Engine) &&
                parts.GuessedUpon.All(x => ((Signature)x).__Hidden_aircraft.Hull == sig.__Hidden_aircraft.Hull) &&
                parts.GuessedUpon.All(x => ((Signature)x).__Hidden_aircraft.Wing == sig.__Hidden_aircraft.Wing)))
            {
                return 0;
            }

            int engineVal = EvaluateSelection<IEngine>(sig.__Hidden_aircraft.Engine, parts.Engine, KnownCollections.OrderOfEngines);
            int hullVal = EvaluateSelection<IHull>(sig.__Hidden_aircraft.Hull, parts.Hull, KnownCollections.OrderOfHulls);
            int wingsVal = EvaluateSelection<IWing>(sig.__Hidden_aircraft.Wing, parts.Wing, KnownCollections.OrderOfWings);

            int foundCount = new int[] { engineVal, hullVal, wingsVal }.Count(x => x != -1);
            double endingScore = 0;

            if (foundCount == 0)
                return 0.5;

            if (engineVal != -1)
            {
                endingScore += 1.0 - ((double)engineVal) / KnownCollections.OrderOfEngines.Count;
            }

            if (hullVal != -1)
            {
                endingScore += 1.0 - ((double)hullVal) / KnownCollections.OrderOfHulls.Count;
            }

            if (wingsVal != -1)
            {
                endingScore += 1.0 - ((double)wingsVal) / KnownCollections.OrderOfWings.Count;
            }

            return endingScore / foundCount;
        }

        private int EvaluateSelection<T>(T expected, T part, IList<T> order) where T : class
        {
            if (part != null)
            {
                int indexOfExpected = order.IndexOf(order.First(x => x.GetType() == expected.GetType()));
                int indexOfPart = order.IndexOf(order.First(x => x.GetType() == part.GetType()));

                return Math.Abs(indexOfExpected - indexOfPart);
            }

            return -1;
        }

        private double ProcessGuess(BlackboardEntry entry)
        {
            GuessAircraft aircraft = entry.data as GuessAircraft;

            int value = EvaluateSelection<IAircraft>((aircraft.guessedUponSig as Signature).__Hidden_aircraft, 
                aircraft.aircraft, KnownCollections.OrderOfCrafts);

            string result = aircraft.aircraft.GetType().Name == (aircraft.guessedUponSig as Signature).__Hidden_aircraft.GetType().Name ? "PASSED" : "FAILED";

            l.log.Debug(String.Format("-- MODERATOR ---- Testing guessing of aircraft <<{0}>> from signature of aircraft <<{1}>>, " + result, 
                aircraft.aircraft.GetType().Name, (aircraft.guessedUponSig as Signature).__Hidden_aircraft.GetType().Name));

            if (!Results.ContainsKey(entry.ModuleName))
                Results.Add(entry.ModuleName, new ExpertData());

            if (result == "PASSED")
                Results[entry.ModuleName].Passed++;
            else
                Results[entry.ModuleName].Failed++;


            return 1.0 - (value / KnownCollections.OrderOfCrafts.Count());
        }
    }
}
