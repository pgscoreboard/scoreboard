﻿/* Copyright (c) 2015, pg-scoreboard
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
using PG_Scoreboard.Interfaces;
using PG_Scoreboard.Interfaces.IBlackboardParameters.Base;
using PG_Scoreboard.NinjectConfig;
using PG_Scoreboard.ScoreboardImpl;
using PG_Scoreboard.ScoreboardImpl.Arbiter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace scoreboard.aircraftAnalyzer.SpecialExperts
{
    class Arbiter : BaseArbiter
    {
        public Arbiter(IExpert[] experts, [Obligatory] IExpert[] obligatory, [Competitive] IExpert[] competitives, IScoreboard<ScoreboardEntry> scoreboard)
            : base(scoreboard, experts, obligatory, competitives)
        {

        }
        
        public override List<IExpert> GetActiveExperts()
        {
            return experts.ToList();
        }

        /// <summary>
        /// Returns all experts that are allowed to activate, regardless if they have anything
        /// to add.
        /// </summary>
        public List<IExpert> GetActiveExperts(IExpert[] experts)
        {
            List<ScoreboardEntry> entries = scoreboard.GetEntries();

            List<IExpert> toActivate = new List<IExpert>();

            foreach (IExpert expert in experts)
            {
                var scoreEntry = entries.Find(x => x.ScoredName == expert.GetName());

                if (scoreEntry == null ||
                    (scoreEntry.Score >= 0.6f))
                    toActivate.Add(expert);
            }

            return toActivate;
        }
        
        public override List<IExpert> GetActiveExperts(string type, int max = int.MaxValue, int min = 0)
        {
            return this.experts.ToList();
        }

        public override List<IExpert> GetActiveExperts(string type)
        {
            IExpert[] experts = this.experts.Where(x => x.processesPhase(type)).ToArray();

            return GetActiveExperts(experts);
        }
    }
}
