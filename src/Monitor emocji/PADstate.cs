﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monitor_emocji
{
    public class PADstate
    {
        public float P { get; set; }
        public float A { get; set; }
        public float D { get; set; }

        public float w_P { get; set; }
        public float w_A { get; set; }
        public float w_D { get; set; }
        public PADstate() { }
        public PADstate(float p, float a, float d,float w)
        {
            this.P = p;
            this.A = a;
            this.D = d;

            this.w_P = w;
            this.w_A = w;
            this.w_D = w;
        }

        public PADstate(float p, float a, float d, float wp,float wa,float wd)
        {
            this.P = p;
            this.A = a;
            this.D = d;

            this.w_P = wp;
            this.w_A = wa;
            this.w_D = wd;
        }

        public PADstate(SinglePADstate P,SinglePADstate A, SinglePADstate D)
        {
            if (P != null)
            {
                this.P = P.State;
                this.w_P = P.w_State;
            }
            if (A != null)
            {
                this.A = A.State;
                this.w_A = A.w_State;
            }
            if (D != null)
            {
                this.D = D.State;
                this.w_D = D.w_State;
            }
        }

        public SinglePADstate GetSingleP()
        {
            return new SinglePADstate() { State = P, w_State = w_P };
        }
        public SinglePADstate GetSingleA()
        {
            return new SinglePADstate() { State = A, w_State = w_A };
        }
        public SinglePADstate GetSingleD()
        {
            return new SinglePADstate() { State = D, w_State = w_D };
        }

        public string Display()
        { 
            return "P: "+P+"\n A: "+A+"\n D: "+D+"\n wP:"+w_P+"\n w_A: "+w_A +"\n w_D:"+w_D;
        }

        public string ShortDisplay()
        {
            return P + ";" + A + ";" + D + ";\n" ;
        }


    }


}
