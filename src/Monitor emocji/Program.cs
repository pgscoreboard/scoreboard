﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Monitor_emocji.Experts;
using PG_Scoreboard.BlackboardImpl;
using PG_Scoreboard.Controllers;
using PG_Scoreboard.Interfaces;
using PG_Scoreboard.Interfaces.IBlackboardParameters.Base;
using PG_Scoreboard.NinjectConfig;
using System.Configuration;
using System.Threading;
using System.Diagnostics;
using PG_Scoreboard.ScoreboardImpl;

namespace Monitor_emocji
{

    class Program
    {
        private void DisplayTime(Stopwatch stopWatch)
        {
            // Get the elapsed time as a TimeSpan value.
            TimeSpan ts = stopWatch.Elapsed;

            // Format and display the TimeSpan value.
            string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
                ts.Hours, ts.Minutes, ts.Seconds,
                ts.Milliseconds / 10);
            Console.WriteLine("RunTime " + elapsedTime);
        }

        static void Main(string[] args)
        {
            while (true)
            {
                ScoreboardController controller = PG_Scoreboard.Utils.ProcessScript.RunScoreboard();
                var ModeratorResult = controller.blackboard.GetEntries().Where(x => x.ModuleName == "ModeratorExpert");
                var Scores = controller.scoreboard.GetEntries();
                foreach (ScoreboardEntry entry in Scores.OrderBy(x => x.ScoredName))
                    Console.WriteLine(entry.ScoredName + ": "+ entry.Score + " Licznik: " + entry.CallNumber);

                foreach (BlackboardEntry entry in ModeratorResult)
                    Console.WriteLine(((PADstate)entry.data).Display()+entry.ModuleName);
            }
        }
    }
}
