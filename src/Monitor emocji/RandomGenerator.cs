﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monitor_emocji
{
    public class RandomGenerator
    {
        private Random random = new Random((int)DateTime.Now.Ticks & 0x0000FFFF);

           private static RandomGenerator instance;

           private RandomGenerator() { }

           public static RandomGenerator Instance
           {
              get 
              {
                 if (instance == null)
                 {
                     instance = new RandomGenerator();
                 }
                 return instance;
              }
           }

        public double NextDouble()
        {
            return random.NextDouble();
        }
    }
}
