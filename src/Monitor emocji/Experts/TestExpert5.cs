﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PG_Scoreboard.BlackboardImpl;
using PG_Scoreboard.Interfaces;

namespace Monitor_emocji.Experts
{
    //zly
    public class TestExpert5 : Expert1
    {
        public TestExpert5(IBlackboard blk)
            : base(blk)
        {
        }

        override public PADstate GiveResult()
        {
            return new PADstate((float)random.NextDouble() - 1,
                                (float)random.NextDouble() - 1,
                                (float)random.NextDouble() - 1,
                                (float)random.NextDouble());


        }
    }
}
