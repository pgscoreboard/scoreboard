﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PG_Scoreboard.BlackboardImpl;
using PG_Scoreboard.Interfaces;
using PG_Scoreboard.Interfaces.IBlackboardParameters;
using PG_Scoreboard.Interfaces.IBlackboardParameters.Base;
using PG_Scoreboard.ScoreboardImpl;
using PG_Scoreboard.ScoreboardImpl.Moderator;

namespace Monitor_emocji.Experts
{
    public class ScoreboardModerator : Expert1,IModerator
    {
         public ScoreboardModerator(IBlackboard blk)
            : base(blk)
        {
        }

        IScoreboard<ScoreboardEntry> scoreboard;
		
		public ScoreboardModerator(IBlackboard blackboard, 
		                     IScoreboard<ScoreboardEntry> scoreboard) 
			: base(blackboard)
		{
			this.scoreboard = scoreboard;
            alreadyProcessed = new List<BlackboardEntry>();
		}
		
		
        private List<BlackboardEntry> alreadyProcessed;
        
        public SinglePADstate AgreedVersion(List<SinglePADstate> list)
        {
            if (list.Count == 0)
                return null;

            float state=0;
            foreach (SinglePADstate singleState in list)
                state += singleState.State;
            state /= list.Count;

            return new SinglePADstate() { State = state, w_State = 1 };
        }

        
		public override void DoWork()
		{
            var entries = this
                .blackboard
                .GetEntries()
                .Except(alreadyProcessed);
            if (entries.Count() > 1)
            {
                List<SinglePADstate> listP = new List<SinglePADstate>();
                List<SinglePADstate> listA = new List<SinglePADstate>();
                List<SinglePADstate> listD = new List<SinglePADstate>();
                foreach (var entry in entries)
                {
                    
                    listP.Add(((PADstate)entry.data).GetSingleP());
                    listA.Add(((PADstate)entry.data).GetSingleA());
                    listD.Add(((PADstate)entry.data).GetSingleD());
                }

                PADstate FinalState = new PADstate(
                    AgreedVersion(listP),
                    AgreedVersion(listA),
                    AgreedVersion(listD)
                    );
                var test2 = new BlackboardEntry(FinalState, GetName(), "FinalState", 1);
                blackboard.AddEntry(test2);
                alreadyProcessed.Add(test2);

                /* Oceny */
                foreach (var entry in entries)
                {
                    float result = (1 - (Math.Abs(FinalState.P - ((PADstate)entry.data).P)) * ((PADstate)entry.data).w_P)
                                  + (1 - (Math.Abs(FinalState.A - ((PADstate)entry.data).A)) * ((PADstate)entry.data).w_A)
                                  + (1 - (Math.Abs(FinalState.D - ((PADstate)entry.data).D)) * ((PADstate)entry.data).w_D);
                    result /= 3;
                    ScoreboardEntry score = new ScoreboardEntry(entry.ModuleName,result);
                    scoreboard.AddEntry(score);
                    alreadyProcessed.Add(entry);
                }
            }
        }
		
		public override string GetName()
		{
			return "ModeratorExpert";
		}

        override public bool processesPhase(string phase)
        {
            return true;
        }
	}

















        
    
}
