﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PG_Scoreboard.Interfaces;

namespace Monitor_emocji.Experts
{
    public class Expert2 : Expert1
    {
        public Expert2(IBlackboard blk)
            : base(blk)
        {
        }

        override public PADstate GiveResult()
        {
            return new PADstate((float)random.NextDouble() * 1.9f - 1, (float)random.NextDouble() * 1.9f - 1, (float)random.NextDouble() * 1.9f - 1, (float)random.NextDouble());
        }
    }
}
