﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PG_Scoreboard.Interfaces;

namespace Monitor_emocji.Experts
{
    public class Expert7 : Expert1
    {
        public Expert7(IBlackboard blk)
            : base(blk)
        {
        }

        override public PADstate GiveResult()
        {
            return new PADstate((float)random.NextDouble() * -0.75f, (float)random.NextDouble() * -0.75f, (float)random.NextDouble() * -0.75f, (float)random.NextDouble());
        }
    }
}
