﻿using PG_Scoreboard.Interfaces;
using PG_Scoreboard.Interfaces.Base;
using PG_Scoreboard.Interfaces.IBlackboardParameters;
using PG_Scoreboard.Interfaces.IExpertParameters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using PG_Scoreboard.BlackboardImpl;
using PG_Scoreboard.Interfaces.IBlackboardParameters.Base;
using PG_Scoreboard.Interfaces.IExpertParameters.Base;
using System.Threading;

namespace Monitor_emocji.Experts
{
    public class Expert1 : BaseExpert
    {
        //public RandomGenerator random;
        public Random random;
        public int count = 1;
        public static int delay = 0;

        public Expert1()
        {
            this.condition = new BaseWorkCondition(x => RunCondition(x));
            random = new Random((int)DateTime.Now.Ticks & 0x0000FFFF);
        }

        public Expert1(IBlackboard blk)
            : base(blk)
        {
            this.condition = new BaseWorkCondition(x => RunCondition(x));
            random = new Random((int)DateTime.Now.Ticks & 0x0000FFFF);
        }

        virtual public PADstate GiveResult()
        {
            return new PADstate((float)random.NextDouble() * 2 - 1, (float)random.NextDouble() * 2 - 1, (float)random.NextDouble() * 2 - 1, (float)random.NextDouble());
        }

        virtual public int RunCondition(List<BlackboardEntry> entries)
        {
            if (entries.Where(x => x.ModuleName == GetName()).Count() > 0)
                return 1; //jeśli się wypowiedziałem, milczę
            else return 0;
        }
        
        override public bool processesPhase(string phase)
        {
            if (phase == "PROCESS")
                return true;
            else return false;
        }

        override public void DoWork()
        {
            String name = GetName();
            Thread.Sleep(delay);
            PADstate test = GiveResult();
            var test2 = new BlackboardEntry(test,GetName(), "State", 1);
            blackboard.AddEntry(test2);
        }
    }
}
