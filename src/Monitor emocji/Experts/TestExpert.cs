﻿using PG_Scoreboard.Interfaces;
using PG_Scoreboard.Interfaces.Base;
using PG_Scoreboard.Interfaces.IBlackboardParameters;
using PG_Scoreboard.Interfaces.IExpertParameters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using PG_Scoreboard.BlackboardImpl;
using PG_Scoreboard.Interfaces.IBlackboardParameters.Base;
using PG_Scoreboard.Interfaces.IExpertParameters.Base;
using Ninject.Planning.Bindings;
using Ninject;

namespace Monitor_emocji.Experts
{
    public class TestExpert : BaseExpert2
    {
        //public RandomGenerator random;
        public Random random;
        public int count = 1;
/*
        public TestExpert()
        {
            this.condition = new BaseWorkCondition(x => InitCondition(x));
            random = new Random((int)DateTime.Now.Ticks & 0x0000FFFF);

        }
        */
        [Inject]
        public TestExpert(IBlackboard blk,IBindingMetadata meta)
            : base(blk, meta)
        {
            this.condition = new BaseWorkCondition(x => InitCondition(x));
            // random = RandomGenerator.Instance;
            random = new Random((int)DateTime.Now.Ticks & 0x0000FFFF);
        }

        virtual public PADstate GiveResult()
        {
            return new PADstate((float)random.NextDouble() * 2 - 1, (float)random.NextDouble() * 2 - 1, (float)random.NextDouble() * 2 - 1, (float)random.NextDouble());
        }

        public int InitCondition(List<BlackboardEntry> entries)
        {
            /*var available = entries.Where(x => x.Type.CompareTo("Test") == 0 ||
                                          x.Type.CompareTo("Test2") == 0);*/
            // available.Count() > 0 ? 0 : 1;
            string debug = GetName();

            if (entries.Where(x => x.ModuleName == GetName()).Count() > 0)
                return 1; //jeśli się wypowiedziałem, milczę
            else return 0;
            /*            string debug = 

                        if (count > 0)
                        {
                            count--;
                            return 0;
                        }
                        else return 1;*/
        }

        override public bool processesPhase(IBlackboardEntryType<string> phase)
        {
            //if (count--==0)
            return true;
            //else return false;
        }

        override public void DoWork()
        {
            PADstate test = GiveResult();
            var test2 = new BlackboardEntry(test, GetName(), "State", 1);
            blackboard.AddEntry(test2);
        }
    }
}
