﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PG_Scoreboard.BlackboardImpl;
using PG_Scoreboard.Interfaces;

namespace Monitor_emocji.Experts
{
    public class TestExpert1 : Expert1
    {
        public TestExpert1(IBlackboard blk)
            : base(blk)
        {
        }

        override public PADstate GiveResult()
        {
            return new PADstate((float)random.NextDouble() * 0.6f + 0.2f,
                                (float)random.NextDouble() * 0.6f + 0.2f,
                                (float)random.NextDouble() * 0.6f + 0.2f,
                                (float)random.NextDouble());


        }
    }
}
