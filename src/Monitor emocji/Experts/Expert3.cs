﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PG_Scoreboard.Interfaces;

namespace Monitor_emocji.Experts
{
    public class Expert3 :Expert1
    {
        public Expert3(IBlackboard blk)
            : base(blk)
        {
        }
        override public PADstate GiveResult()
        {
            return new PADstate((float)random.NextDouble() * 1.8f - 0.95f, (float)random.NextDouble() * 1.9f - 0.95f, (float)random.NextDouble() * 1.9f - 0.95f, (float)random.NextDouble());
        }

        
    }
}
