﻿/* Copyright (c) 2015, pg-scoreboard
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Ninject.Modules;
using PG_Scoreboard.Interfaces;
using PG_Scoreboard.Interfaces.IBlackboardParameters;
using PG_Scoreboard.Interfaces.IBlackboardParameters.Base;
using PG_Scoreboard.Repository;
using PG_Scoreboard.Utils;

namespace PG_Scoreboard.BlackboardImpl
{
    /// <summary>
    /// Blackboard is a singleton object, used to store <see cref="BlackBoardEntry"/>
    /// While running application experts generate entries which are placed on this board.
    /// </summary>
    public class Blackboard : IBlackboard
    {
        private int currentTurn = 1;
        private int currentPhase = 1;

        /// <summary>
        /// Funtion used to get number of turn which is currently in progress
        /// </summary>
        /// <returns>Current Turn - possitive integer which value is equal or above 1</returns>
        public int GetCurrentTurn()
        {
            return currentTurn;
        }

        /// <summary>
        /// Function used to get number of phase which is currently in progress
        /// </summary>
        /// <returns>Current Phase - possitive integer which value is equal or above 1</returns>
        public int GetCurrentPhase()
        {
            return currentPhase;
        }

        /// <summary>
        /// Content is a list of <see cref="BlackboardEntries"/> from specified type (T)
        /// </summary>
        public List<BlackboardEntry> content { get; private set; }

        private IEntryRepository<BlackboardEntry> entryRepository = RepositoryFactory.Get();
        
        /// <summary>
        /// Simple Constructor which creates empty content - list of entries
        /// </summary>
        public Blackboard()
        {
            content = new List<BlackboardEntry>();
        }

        /// <summary>
        /// Function used to add entry to the Blackboard.
        /// Function set current phase and turn to entry, save entry to content list and also save entry in defined SQLite database or XML file
        /// To chose where to save entries there's a need to declare attributes in config file.
        /// Attributes:
        /// "BlackboardMode" should have value "DATABASE" or "XMLFILE"
        /// "PathBlackboard" have value which is the path to file - to database file or xml file
        /// </summary>
        /// <param name="entry">entry which will be added<see cref=" BlackBoardEntry"/></param>
        public void AddEntry(BlackboardEntry entry)
        {
            entry.setPhase(currentPhase);
            entry.setTurn(currentTurn);
            content.Add(entry);
            entryRepository.Save(entry);
        }

        /// <summary>
        /// Function used to get all entries of type T which are in the memory.
        /// Condition which entries should be in the memory are set in the config file.
        /// Attributes:
        /// "MaxTurnsInTheMemory" - maximum of full turns in the memory
        /// "MaxPhasesInTheMemory" - maximum of full phases in the memory
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="type">type of entries</param>
        /// <returns>each entry from blackboard of declared type</returns>
        public List<BlackboardEntry> GetEntries<T>(IBlackboardEntryType<T> type)
        {
            return content.Where(x => x.Type.CompareTo((SimpleBlackboardEntryType)type) == 0).ToList();
        }

        /// <summary>
        /// Function used to get all entries created by module with declared name and of declared type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="moduleName">name of module</param>
        /// <param name="type">type of entries</param>
        /// <returns>each entry from blackboard memory of declared type and created by declared module</returns>
        public List<BlackboardEntry> GetEntries<T>(string moduleName, IBlackboardEntryType<T> type)
        {
            return content.Where(
                x => x.Type.CompareTo((SimpleBlackboardEntryType)type) == 0 &&
                     x.ModuleName != moduleName).ToList();
        }

        /// <summary>
        /// Function used to get all entries from the memory.
        /// Condition which entries should be in the memory are set in the config file.
        /// Attributes:
        /// "MaxTurnsInTheMemory" - maximum of full turns in the memory
        /// "MaxPhasesInTheMemory" - maximum of full phases in the memory
        /// </summary>
        /// <returns>list of all entries from blackboard</returns>
        public List<BlackboardEntry> GetEntries()
        {
            return content;
        }

        /// <summary>
        ///Function to remove Entry from blackboard
        /// </summary>
        /// <param name="entry">entry which will be removed</param>
        public void RemoveEntry(BlackboardEntry entry)
        {
            content.Remove(entry);
        }

        /// <summary>
        /// Function used to cleared all entries from blackboard memory, not from database or xml file
        /// </summary>
        public void ClearEntry()
        {
            content = new List<BlackboardEntry>();
        }

        /// <summary>
        /// Function used to increase phase of application and set turn to started value "1"
        /// </summary>
        public void ChangePhase()
        {
            currentPhase += 1;
            currentTurn = 1;
        }

        /// <summary>
        /// Function used to increase number of turn in phase and remove additional entries.
        /// Entries are limited in the config file by attributes:
        /// "MaxTurnsInTheMemory" - maximum of full turns in the memory
        /// "MaxPhasesInTheMemory" - maximum of full phases in the memory
        /// </summary>
        public void ChangeTurn()
        {
            removeAdditionalEntries();

            currentTurn += 1;
        }

        /// <summary>
        /// Function to get current turn and phase numbers by object <see cref=" PhaseTurnNumber"/>
        /// </summary>
        /// <returns>Returns PhaseTurnNumber object</returns>
        public PhaseTurnNumber GetPhaseAndTurn()
        {
            return new PhaseTurnNumber(currentPhase, currentTurn);
        }

        private void removeAdditionalEntries()
        {
            HashSet<PhaseTurnNumber> phaseTurnNumbers = new HashSet<PhaseTurnNumber>();

            foreach (BlackboardEntry entry in content)
            {
                phaseTurnNumbers.Add(new PhaseTurnNumber(entry.phase, entry.turn));
            }

            while (isTooMuch(phaseTurnNumbers))
            {
                PhaseTurnNumber turnToRemove = phaseTurnNumbers.First();

                List<BlackboardEntry> additionalEntries = content.Where(x => x.phase == turnToRemove.phase && x.turn == turnToRemove.turn).ToList();
                phaseTurnNumbers.Remove(turnToRemove);

                foreach (BlackboardEntry entry in additionalEntries)
                {
                    content.Remove(entry);
                }
            }
        }

        private Boolean isTooMuch(HashSet<PhaseTurnNumber> phaseTurnNumbers)
        {
            if (Configuration.MaxPhasesInTheMemory == 0 || Configuration.MaxTurnsInTheMemory == 0)
            {
                return false;
            }

            return phaseTurnNumbers.Count > Configuration.MaxTurnsInTheMemory && phaseTurnNumbers.Last().phase - phaseTurnNumbers.First().phase + 1 > Configuration.MaxPhasesInTheMemory;
        }
    }

    /// <summary>
    /// Object of class contains current phase and turn number
    /// </summary>
    public class PhaseTurnNumber
    {
        public int phase { get; set; }
        public int turn { get; set; }

        /// <summary>
        /// Constructor which creates object with declared phase's and turn's numbers
        /// </summary>
        /// <param name="phase">phase number</param>
        /// <param name="turn">turn number</param>
        public PhaseTurnNumber(int phase, int turn)
        {
            this.phase = phase;
            this.turn = turn;
        }

        /// <summary>
        /// Overrided function to equals object of this class
        /// </summary>
        /// <param name="obj"></param>
        /// <returns>true or false - if objects are equals or not</returns>
        public override bool Equals(Object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            PhaseTurnNumber other = (PhaseTurnNumber)obj;
            return turn == other.turn && phase == other.phase;
        }

        /// <summary>
        /// Overrided function returning hash code of object
        /// </summary>
        /// <returns>hash code</returns>
        public override int GetHashCode()
        {
            int hash = 17;
            hash = 89 * hash + phase.GetHashCode();
            hash = 89 * hash + turn.GetHashCode();
            return hash;
        }
    }
}
