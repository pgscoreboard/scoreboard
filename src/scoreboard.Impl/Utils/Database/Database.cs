﻿/* Copyright (c) 2015, pg-scoreboard
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
using PG_Scoreboard.BlackboardImpl;
using PG_Scoreboard.ScoreboardImpl;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.IO;

namespace PG_Scoreboard.Utils
{
    /// <summary>
    /// In this class there are function and parameters needed for saving entries to the database
    /// </summary>
    /// <typeparam name="T">determines if this should be database of scoreboard or blackboard</typeparam>
    public class Database<T>
    {
        /// <summary>
        /// If there's no path given in the config, database file will be created at this path
        /// </summary>
        public static String DEFAULT_PATH = "./DefaultDatabase.sqlite";

        private static String SCOREBOARD_TABLE = "ScoreboardScore";
        private static String BLACKBOARD_TABLE = "BlackboardEntry";

        private static String CREATE_ENTRY_TABLE = "create table if not exists " + BLACKBOARD_TABLE + " (timestamp varchar(50) PRIMARY KEY, module_name VARCHAR(100),type VARCHAR(100), assurance REAL, data BLOB ) ";
        private static String CREATE_SCOREBOARD_TABLE = "create table if not exists " + SCOREBOARD_TABLE + " (scored_name VARCHAR(100) PRIMARY KEY,score REAL, number_of_calls INTEGER ) ";

        private static String DB_FULL_PATH = DEFAULT_PATH;
        private static Logger log = new Logger(typeof(Database<T>));

        /// <summary>
        /// Funciton to save ScoreboardEntry to the database
        /// </summary>
        /// <param name="entry">entry which should be saved</param>
        public static void SaveEntry(ScoreboardEntry entry)
        {
            CheckIfTableExists();

            using (SQLiteConnection dbConnection = new SQLiteConnection(GetConnectionString()))
            {
                dbConnection.Open();

                List<ScoreboardEntry> upToDateEntry = GetEntries(entry.ScoredName);
                if (upToDateEntry.Count > 0)
                {
                    entry.Score += upToDateEntry[0].Score * upToDateEntry[0].CallNumber;
                    entry.CallNumber = upToDateEntry[0].CallNumber + 1;
                    entry.Score /= entry.CallNumber;
                }

                String insertEntry = "INSERT OR REPLACE INTO " + SCOREBOARD_TABLE + " (scored_name,score,number_of_calls) VALUES (@scoredName,@score,@numberOfCalls)";
                SQLiteCommand command = new SQLiteCommand(insertEntry, dbConnection);
                command.Parameters.AddWithValue("@scoredName", entry.ScoredName);
                command.Parameters.AddWithValue("@score", entry.Score);
                command.Parameters.AddWithValue("@numberOfCalls", entry.CallNumber);
                command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Funciton to save BlackboardEntry to the database
        /// </summary>
        /// <param name="entry">entry which should be saved</param>
        public static void SaveEntry(BlackboardEntry entry)
        {
            CheckIfTableExists();

            using (SQLiteConnection dbConnection = new SQLiteConnection(GetConnectionString()))
            {
                dbConnection.Open();
                String insertEntry = "INSERT INTO " + BLACKBOARD_TABLE + " (timestamp, module_name, type, assurance, data) VALUES (@time, @module, @type, @assurance, @data)";
                SQLiteCommand command = new SQLiteCommand(insertEntry, dbConnection);
                command.Parameters.AddWithValue("@time", entry.TimeStamp);
                command.Parameters.AddWithValue("@module", entry.ModuleName);
                command.Parameters.AddWithValue("@type", entry.Type);
                command.Parameters.AddWithValue("@assurance", entry.Assurance);
                command.Parameters.AddWithValue("@data", entry.data);

                command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Function to search entries in the scoreboard database
        /// If function is call without key parameter, all entries will be return
        /// </summary>
        /// <param name="key">name of score which will be search</param>
        /// <returns>list of found <see cref="ScoreboardEntry"/></returns>
        public static List<ScoreboardEntry> GetEntries(String key = "")
        {
            List<ScoreboardEntry> entries = new List<ScoreboardEntry>();
            String selectAll;
            CheckIfTableExists();

            using (SQLiteConnection dbConnection = new SQLiteConnection(GetConnectionString()))
            {
                dbConnection.Open();

                selectAll = "SELECT * FROM " + SCOREBOARD_TABLE;

                if (!String.IsNullOrEmpty(key))
                    selectAll += " WHERE scored_name = " + "'" + key + "'";

                SQLiteCommand command = new SQLiteCommand(selectAll, dbConnection);

                SQLiteDataReader reader = command.ExecuteReader();
                while (reader.Read())//Depth??
                {
                    entries.Add(new ScoreboardEntry(Convert.ToString(reader["scored_name"]), Convert.ToDouble(reader["score"]), Convert.ToInt32(reader["number_of_calls"])));
                }

            }

            return entries;
        }

        private static void CheckIfTableExists()
        {
            if (SetProperDbPath())
            {
                if (!File.Exists(DB_FULL_PATH))
                {
                    SQLiteConnection.CreateFile(DB_FULL_PATH);
                }

                CreateTable();

            }
        }

        private static void CreateTable()
        {
            if (typeof(T).Equals(typeof(BlackboardEntry)))
            {
                CreateTable(CREATE_ENTRY_TABLE);
            }
            else if (typeof(T).Equals(typeof(ScoreboardEntry)))
            {
                CreateTable(CREATE_SCOREBOARD_TABLE);
            }
        }

        private static Boolean SetProperDbPath()
        {
            if (typeof(T).Equals(typeof(BlackboardEntry)))
            {
                DB_FULL_PATH = Configuration.BlackboardPath;
            }
            else if (typeof(T).Equals(typeof(ScoreboardEntry)))
            {
                DB_FULL_PATH = Configuration.ScoreboardPath;
            }
            else
            {
                log.log.Error("Database type not supported");
                return false;
            }
            if (String.IsNullOrEmpty(DB_FULL_PATH))
            {
                DB_FULL_PATH = DEFAULT_PATH;
            }

            return true;
        }

        private static void CreateTable(String createTableString)
        {
            using (SQLiteConnection dbConnection = new SQLiteConnection(GetConnectionString()))
            {
                dbConnection.Open();
                string createDbSchema = createTableString;
                SQLiteCommand command = new SQLiteCommand(createDbSchema, dbConnection);
                command.ExecuteNonQuery();

                command.ExecuteNonQuery();
            }
        }

        private static String GetConnectionString()
        {
            return "Data Source=" + DB_FULL_PATH + ";Version=3;";
        }
    }
}