﻿/* Copyright (c) 2015, pg-scoreboard
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PG_Scoreboard.Utils.Enums
{
    /// <summary>
    /// Enum with types of entries storage
    /// </summary>
    public enum RepositoryType
    {
        DATABASE,
        XMLFILE
    }

    /// <summary>
    /// Class with functions and variables needed to operate extensions
    /// </summary>
    public static class RepositoryTypeExtensions
    {
        private static String DATABASE_EXTENSION = ".sqlite";
        private static String XML_EXTENSION = ".xml";
        private static String DEFAULT_EXTENSION = ".txt";

        /// <summary>
        /// Return the extension of given type
        /// </summary>
        /// <param name="type">type of entries storage</param>
        /// <returns>extension</returns>
        public static String GetExtension(String type)
        {
            if (type.Equals(RepositoryType.DATABASE.ToString()))
            {
                return DATABASE_EXTENSION;
            }
            else if (type.Equals(RepositoryType.XMLFILE.ToString()))
            {
                return XML_EXTENSION;
            }
            else
            {
                return DEFAULT_EXTENSION;
            }
        }
    }
}
