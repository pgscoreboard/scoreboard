﻿/* Copyright (c) 2015, pg-scoreboard
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
using PG_Scoreboard.Utils.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using PG_Scoreboard.Interfaces.IBlackboardParameters.Base;
using System.IO;
using PG_Scoreboard.NinjectConfig;
using PG_Scoreboard.BlackboardImpl;

namespace PG_Scoreboard.Utils
{
    /// <summary>
    /// Class which take care of attributes' values from config file
    /// </summary>
    public static class Configuration
    {
        static Configuration()
        {
            String blackboardPathCandidate = System.Configuration.ConfigurationManager.AppSettings["PathBlackboard"];
            String scoreboardPathCandidate = System.Configuration.ConfigurationManager.AppSettings["PathScoreboard"];
            String defaultPath = Database<BlackboardEntry>.DEFAULT_PATH;

            //Init Value:
            int.TryParse(System.Configuration.ConfigurationManager.AppSettings["MaxPhasesInTheMemory"], out MAX_PHASES_IN_THE_MEMORY);
            int.TryParse(System.Configuration.ConfigurationManager.AppSettings["MaxTurnsInTheMemory"], out MAX_TURNS_IN_THE_MEMORY);

            BLACKBOARD_PATH = checkPath(blackboardPathCandidate) ? blackboardPathCandidate : defaultPath;
            SCOREBOARD_PATH = checkPath(scoreboardPathCandidate) ? scoreboardPathCandidate : defaultPath;

            if (File.Exists(ConfigurationManager.AppSettings["ConfigPath"]) && string.IsNullOrWhiteSpace(ConfigFile))
                ConfigFile = ConfigurationManager.AppSettings["ConfigPath"];

            if (System.Configuration.ConfigurationManager.AppSettings["BlackboardMode"] != "XMLFILE" &&
               System.Configuration.ConfigurationManager.AppSettings["BlackboardMode"] != "DATABASE")
                BLACKBOARD_REPOSITORY_TYPE = "XMLFILE";
            else BLACKBOARD_REPOSITORY_TYPE = System.Configuration.ConfigurationManager.AppSettings["BlackboardMode"];

            if (!int.TryParse(ConfigurationManager.AppSettings["MaxExpertsNumber"], out MAX_EXPERTS_NUMBER))
                MAX_EXPERTS_NUMBER = 100;
            if (!int.TryParse(ConfigurationManager.AppSettings["MinExpertsNumber"], out MIN_EXPERTS_NUMBER))
                MIN_EXPERTS_NUMBER = 0;
            if (!int.TryParse(ConfigurationManager.AppSettings["MaxRoundInPhaseNumber"], out MAX_ROUND_IN_PHASE_NUMBER))
                MAX_ROUND_IN_PHASE_NUMBER = 100;
            if (!int.TryParse(ConfigurationManager.AppSettings["MinExpertExecutes"], out MIN_EXPERT_EXECUTES))
                MIN_EXPERT_EXECUTES = 5;

            if (stages == null)
            {
                stages = new List<string>();
                if (!string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["Stages"]))
                    foreach (string stage in ConfigurationManager.AppSettings["Stages"].Split(';'))
                        stages.Add(stage);
            }

            BLACKBOARD_PATH = checkBlackboardPath(BLACKBOARD_PATH, BLACKBOARD_REPOSITORY_TYPE);
        }


        private static String BLACKBOARD_PATH;
        private static String SCOREBOARD_PATH;
        private static String BLACKBOARD_REPOSITORY_TYPE;
        private static int MAX_ROUND_IN_PHASE_NUMBER;
        private static int MAX_EXPERTS_NUMBER;
        private static int MIN_EXPERTS_NUMBER;
        private static int MIN_EXPERT_EXECUTES;
        private static int MAX_PHASES_IN_THE_MEMORY;
        private static int MAX_TURNS_IN_THE_MEMORY;
       

        private static string CONFIG_FILE;
       
        /// <summary>
        /// Path where blackboard entries will be storage
        /// </summary>
        public static string BlackboardPath { get { checkBlackboardPath(BLACKBOARD_PATH, BLACKBOARD_REPOSITORY_TYPE); return BLACKBOARD_PATH; } }
        /// <summary>
        /// Path where scoreboard entries will be storage
        /// </summary>
        public static string ScoreboardPath { get { return SCOREBOARD_PATH; } }
        /// <summary>
        /// Type of blackboard entries storage
        /// </summary>
        public static string BlackboardRepositoryType { get { return BLACKBOARD_REPOSITORY_TYPE; } }
        /// <summary>
        /// Maximym number of turns in the phase
        /// </summary>
        public static int MaxRoundInPhaseNumber { set { MAX_ROUND_IN_PHASE_NUMBER = value; } get { return MAX_ROUND_IN_PHASE_NUMBER; } }       
        /// <summary>
        /// Maximum number of calls experts
        /// </summary>
        public static int MaxExpertsNumber { set { MAX_EXPERTS_NUMBER = value; } get { return MAX_EXPERTS_NUMBER; } }
        /// <summary>
        /// Minimum number of calls experts 
        /// </summary>
        public static int MinExpertsNumber { set { MIN_EXPERTS_NUMBER = value; } get { return MIN_EXPERTS_NUMBER; } }
        //For BaseArbiterOnly
        public static int MinimalExpertExecutes { set { MIN_EXPERT_EXECUTES = value; } get { return MIN_EXPERT_EXECUTES; } }
        /// <summary>
        /// Limitation of blackboard entries in the memory - maximum number of phases
        /// </summary>
        public static int MaxPhasesInTheMemory { set { MAX_PHASES_IN_THE_MEMORY = value; } get { return MAX_PHASES_IN_THE_MEMORY; } }
        /// <summary>
        /// Limitation of blackboard entries in the memory - maximum number of turns
        /// </summary>
        public static int MaxTurnsInTheMemory { set { MAX_TURNS_IN_THE_MEMORY = value; } get { return MAX_TURNS_IN_THE_MEMORY; } }
        /// <summary>
        /// Path to the config file with declared experts
        /// </summary>
        public static string ConfigFile { get { return CONFIG_FILE; } set { CONFIG_FILE = value;/* NinjectInterface.LoadFromConfigFile(value);*/ } }

        /// <summary>
        /// List of the stages
        /// </summary>
        public static List<string> stages { get; set; }

        /// <summary>
        /// Function to change path to the blackboard's entries storage 
        /// </summary>
        /// <param name="path">new path</param>
        public static void changeBlackboardPath(String path)
        {
            BLACKBOARD_PATH = path;
        }

        /// <summary>
        /// Function to change path to the scoreboard's database 
        /// </summary>
        /// <param name="path">new path</param>
        public static void changeDbScoreboardPath(String path)
        {
            SCOREBOARD_PATH = path;
        }

        /// <summary>
        /// Function to change type of blackboard storage
        /// </summary>
        /// <param name="type"></param>
        public static void changeBlackboardReporitoryType(RepositoryType type)
        {
            BLACKBOARD_REPOSITORY_TYPE = type.ToString();
        }

        private static Boolean checkPath(String path)
        {
            return Path.GetDirectoryName(path) != null;
        }

        private static String checkBlackboardPath(String blackboardPath, String blackboardRepositoryType)
        {
            String expectedExtension = RepositoryTypeExtensions.GetExtension(blackboardRepositoryType);

            if (!blackboardPath.Contains(expectedExtension))
            {
                if (!Path.HasExtension(blackboardPath))
                {
                    blackboardPath = blackboardPath + expectedExtension;
                }
                else
                {
                    blackboardPath = Path.ChangeExtension(blackboardPath, expectedExtension);
                }
            }

            return blackboardPath;
        }
    }
}
