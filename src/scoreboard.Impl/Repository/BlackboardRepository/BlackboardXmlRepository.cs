﻿/* Copyright (c) 2015, pg-scoreboard
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
using PG_Scoreboard.BlackboardImpl;
using PG_Scoreboard.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace PG_Scoreboard.Repository
{
    /// <summary>
    /// Part of Repository Pattern - implementation of xml repository
    /// </summary>
    public class BlackboardXmlRepository : IEntryRepository<BlackboardEntry>
    {
        private static String FILE_PATH = PG_Scoreboard.Utils.Configuration.BlackboardPath;
        
        private FileInfo fileInfo = new FileInfo(FILE_PATH);

        private const String ROOT_NAME = "Blackboard";
        private const String NODE_NAME = "Entry";
        private const String TIMESTAMP_NAME = "Time";
        private const String MODULETYPE_NAME = "Module";
        private const String ENTRYTYPE_NAME = "Type";
        private const String ASSURANCE_NAME = "Assurance";
        private const String DATA_NAME = "Data";

        /// <summary>
        /// Save entry in the xml file.
        /// Function checks if file exists and creates it if needed
        /// </summary>
        /// <param name="entry">entry to be saved</param>
        public void Save(BlackboardEntry entry)
        {
            CreateFileIfNotExists();

            XDocument entriesDocument;

            try
            {
                entriesDocument = XDocument.Load(FILE_PATH);
            }
            catch (XmlException e)
            {
                entriesDocument = new XDocument(new XElement(ROOT_NAME));
            }

            XElement entryNode = new XElement(NODE_NAME,
               new XElement(TIMESTAMP_NAME, entry.TimeStamp),
               new XElement(MODULETYPE_NAME, entry.ModuleName),
               new XElement(ENTRYTYPE_NAME, entry.Type.ToString()),
               new XElement(ASSURANCE_NAME, entry.Assurance),
               new XElement(DATA_NAME, entry.data));

            entriesDocument.Root.Add(entryNode);
            entriesDocument.Save(FILE_PATH);
        }

        private void CreateFileIfNotExists()
        {
            if (!fileInfo.Directory.Exists)
            {
                fileInfo.Directory.Create();
            }

            if (!File.Exists(FILE_PATH))
            {
                using (File.Create(FILE_PATH))
                {
                }
            }
        }
    }
}
