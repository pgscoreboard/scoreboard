﻿/* Copyright (c) 2015, pg-scoreboard
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
using System;
using System.Collections.Generic;
using Ninject.Modules;
using PG_Scoreboard.BlackboardImpl;
using PG_Scoreboard.Interfaces.IBlackboardParameters;

/*
 * User: Macr
 * Date: 2014-03-17
 * Time: 18:36
 */

namespace PG_Scoreboard.Interfaces
{
	/// <summary>
	/// Interface of blackboard.
    /// Blackboard is a board on which experts places their results.
	/// </summary>
    public interface IBlackboard : IBoard<BlackboardEntry>
	{
        List<BlackboardEntry> GetEntries<T>(IBlackboardEntryType<T> type);
        /// <summary>
        /// Function to get which turn is in progress
        /// </summary>
        /// <returns>number of current turn</returns>
        int GetCurrentTurn();
        
        /// <summary>
        /// Function to get which phase is in progress
        /// </summary>
        /// <returns>number of current phase</returns>
        int GetCurrentPhase();
        
        /// <summary>
        /// returns entries of given type and which was cerated by given module
        /// </summary>
        /// <param name="moduleName">name of module which created entry</param>
        /// <param name="type">type which is wanted</param>
        /// <returns></returns>
        List<BlackboardEntry> GetEntries<T>(string moduleName, IBlackboardEntryType<T> type);
       
        /// <summary>
        /// Function to clear all entries from blackboard memory
        /// </summary>
        void ClearEntry();
       
        /// <summary>
        /// Function to remove given entry
        /// </summary>
        /// <param name="entry">entry which should be removed</param>
        void RemoveEntry(BlackboardEntry entry);
       
        /// <summary>
        /// Function to get to the next turn
        /// </summary>
        void ChangeTurn();
       
        /// <summary>
        /// Function to get to the next phase
        /// </summary>
        void ChangePhase();
       
        /// <summary>
        /// Function to get number of turn and phase
        /// </summary>
        /// <returns>object of <see cref="PhaseTurnNumber"/> which containd turn and phase number</returns>
        PhaseTurnNumber GetPhaseAndTurn();
    }
}
