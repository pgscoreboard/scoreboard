﻿/* Copyright (c) 2015, pg-scoreboard
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * User: Macr
 * Date: 2014-03-24
 * Time: 22:14
 */
using System;
using Ninject;
using Ninject.Planning.Bindings;
using PG_Scoreboard.Exceptions;
using PG_Scoreboard.Interfaces;
using PG_Scoreboard.Interfaces.IBlackboardParameters;
using PG_Scoreboard.Interfaces.IExpertParameters;
using PG_Scoreboard.Interfaces.IExpertParameters.Base;
using PG_Scoreboard.Utils;
using PG_Scoreboard.BlackboardImpl;

namespace PG_Scoreboard.Interfaces.Base
{
	/// <summary>
	/// Base expert, implementing basic functions
	/// </summary>

	public abstract class BaseExpert : IExpert
	{
        private Logger l = new Logger(typeof(BaseExpert));

		public abstract void DoWork();
		
        protected IBlackboard blackboard;
        protected IWorkCondition condition;

		public virtual IWorkCondition GetWorkCondition()
		{
			return condition;
		}
		
		public virtual string GetName()
		{
            return this.ToString();
		}

        public BaseExpert(IBlackboard blackboard)
		{
			this.blackboard = blackboard;
            if (blackboard == null)
                l.log.Warn("Expert: " + GetName() + " doesn't have reference to blackboard");
		}

        public BaseExpert()
        {
            l.log.Warn("Expert: " + GetName() + " doesn't have reference to blackboard");
        }
		
		public virtual bool processesPhase(string phase)
		{
			throw new NotImplementedException();
		}

        public int CompareTo(IExpert expert)
        {
            return this.GetName().CompareTo(expert.GetName());
        }


	}
}
