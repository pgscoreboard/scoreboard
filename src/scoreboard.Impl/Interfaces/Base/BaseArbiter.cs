﻿/* Copyright (c) 2015, pg-scoreboard
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Ninject;
using PG_Scoreboard.Interfaces;
using PG_Scoreboard.Interfaces.IBlackboardParameters.Base;
using PG_Scoreboard.NinjectConfig;
using PG_Scoreboard.ScoreboardImpl;
using PG_Scoreboard.ScoreboardImpl.Arbiter;
using ExtensionMethods;

namespace PG_Scoreboard.Interfaces.Base
{
    /// <summary>
    /// Base implementation of Arbiter
    /// </summary>
    public class BaseArbiter : IArbiter
	{
        private class IExpertWithScore
        {
            public IExpert Expert;
            public double Score;
            public int Call = 0;

            public IExpertWithScore(IExpert expert, List<ScoreboardEntry> entries)
            {
                Expert = expert;
                var scoreEntry = entries.Find(x => x.ScoredName == expert.GetName());
                if (scoreEntry != null)
                {
                    Score = scoreEntry.Score;
                    Call = scoreEntry.CallNumber;
                }
            }
        }

		protected IExpert[] experts;
        protected IExpert[] obligatories;
        protected IExpert[] competitives;
        protected IScoreboard<ScoreboardEntry> scoreboard;
    
        Random random;

        /// <summary>
        /// Create object of BaseArbiter with given parameters
        /// </summary>
        /// <param name="scoreboard">scoreboard object</param>
        /// <param name="experts">array of experts</param>
        /// <param name="obligatory">array of experts with should have voice in the discussion</param>
        /// <param name="competitives">array of experts that could have voice is arbiter says so</param>
        [Inject]
        public BaseArbiter(IScoreboard<ScoreboardEntry> scoreboard,
                           IExpert[] experts,
                           [Obligatory] IExpert[] obligatory,
                           [Competitive]IExpert[] competitives)
        {
            this.experts = experts;
            this.obligatories = obligatory;
            this.competitives = competitives;

            this.scoreboard = scoreboard;
        }

        public virtual List<IExpert> GetActiveExperts()
		{
            List<IExpert> competitive = this.competitives.ToList();
            List<IExpert> experts = this.obligatories.ToList();
            experts.AddRange(competitive);
            return experts;
		}

        public virtual List<IExpert> GetActiveExperts(String type, int max = int.MaxValue, int min = 0)
		{
            int p = PG_Scoreboard.Utils.Configuration.MinimalExpertExecutes;

            random = new Random(DateTime.Now.Millisecond);
            List<ScoreboardEntry> entries = scoreboard.GetEntries();
			List<IExpert> toActivate = new List<IExpert>();
            toActivate.AddRange(obligatories.Where(x => x.processesPhase(type)));
            List<IExpert> mayActivate = competitives.Where(x => x.processesPhase(type)).ToList();
            
            //if we have less experts than the minimum, return all
            if(min >= (toActivate.Count + mayActivate.Count)) 
            {
                toActivate.AddRange(mayActivate);
                return toActivate;
            }
            else
                //if (max > 0)
                {
                    int m = max - toActivate.Count; //max, when we can active
                    List<IExpertWithScore> listExperts = new List<IExpertWithScore>();
                    foreach (IExpert expert in mayActivate)
                        listExperts.Add(new IExpertWithScore(expert,entries));
                    var list = listExperts.Where(x => x.Call <=p);
                    if (list.Count() > m)
                        list = list.OrderBy(x => x.Call).Take(m).ToList();

                    foreach(IExpertWithScore IExpertWithScore in list)
                        toActivate.Add(IExpertWithScore.Expert);
                    if (max != toActivate.Count)
                    {
                        List<IExpertWithScore> RestOfExperts = listExperts.Where(x => x.Call > p).ToList();
                        while (toActivate.Count <= max  && toActivate.Count < min && RestOfExperts.Count > 0)
                        {
                            RestOfExperts.Shuffle();
                            var remove = new List<IExpertWithScore>();
                            foreach (IExpertWithScore IExpertWithScore in RestOfExperts)
                            {
                                if (random.NextDouble() <= IExpertWithScore.Score)
                                {
                                    toActivate.Add(IExpertWithScore.Expert);
                                    remove.Add(IExpertWithScore);
                                }

                                if (max <= toActivate.Count)
                                    break;
                            }
                            foreach(IExpertWithScore IExpertWithScore in remove)
                                RestOfExperts.Remove(IExpertWithScore);
                        }
                    }
                }
            System.IO.File.AppendAllText(@"E:\\Test.txt", toActivate.Count().ToString() + "\n");
			
            return toActivate;
		}

        public virtual List<IExpert> GetActiveExperts(String type)
		{
            return GetActiveExperts(type,PG_Scoreboard.Utils.Configuration.MaxExpertsNumber, PG_Scoreboard.Utils.Configuration.MinExpertsNumber);
        }

    }
}
