﻿/* Copyright (c) 2015, pg-scoreboard
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
using PG_Scoreboard.BlackboardImpl;
using PG_Scoreboard.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PG_Scoreboard.Controllers;
using PG_Scoreboard.Interfaces.IBlackboardParameters.Base;

namespace PG_Scoreboard.Interfaces.Base
{
    /// <summary>
    /// AbstractController is an abstract class which is base for blackboard and scoreboard pattern implementation
    /// </summary>
    public abstract class AbstractController : IController
    {
        protected abstract Logger getLogger();
        protected Logger l = new Logger(typeof(BlackboardController));
        protected List<string> stages;

        /// <summary>
        /// Checks if supplied experts are unique by given names (according to getName)
        /// </summary>
        /// 
        protected IExpert[] GetUnique(IExpert[] experts)
        {
            SortedSet<IExpert> check = new SortedSet<IExpert>(experts);

            if (experts.Length != check.Count)
            {
                getLogger().log.Error("Some experts have same identification name");
                IEnumerable<IExpert> nonunique = experts.Except(check);

                nonunique.ToList().ForEach(x => getLogger().log.Error(String.Format("Expert in error {0} {1}", x.GetName(), x.GetType().FullName)));
            }

            return check.ToArray();
        }

        protected void tryLaunchExpert(IExpert expert, ref bool nothingAdded)
        {
            bool shouldActivate =
                expert.GetWorkCondition().CompareTo(blackboard.GetEntries()) == 0 ? true : false;

            l.log.Debug(expert.GetType() + " " + expert.GetName() + " will activate: " + shouldActivate);

            if (shouldActivate)
            {
                expert.DoWork();
                nothingAdded = false;
            }
        }
        /// <summary>
        /// blackboard object
        /// </summary>
        public abstract IBlackboard blackboard { get; }
        /// <summary>
        /// array of unique experts
        /// </summary>
        public abstract IExpert[] experts { get; }

        /// <summary>
        /// implementation of function is a controller loop
        /// </summary>
        public abstract void run();
    }
}
