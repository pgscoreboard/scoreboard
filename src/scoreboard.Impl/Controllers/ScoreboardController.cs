﻿/* Copyright (c) 2015, pg-scoreboard
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Ninject;
using PG_Scoreboard.Interfaces;
using PG_Scoreboard.Interfaces.IBlackboardParameters;
using PG_Scoreboard.Interfaces.IBlackboardParameters.Base;
using PG_Scoreboard.NinjectConfig;
using PG_Scoreboard.ScoreboardImpl;
using PG_Scoreboard.Utils;
using PG_Scoreboard.Interfaces.Base;
using System.Configuration;
using PG_Scoreboard.BlackboardImpl;

namespace PG_Scoreboard.Controllers
{
    /// <summary>
    /// ScoreboardController is an implementation of a workflow in Scoreboard
    /// pattern.
    /// </summary>
    public class ScoreboardController : AbstractController
    {
        #region variable
        private IExpert[] _obligatories;
        private IExpert[] _competitives;
        protected IModerator _moderator;
        protected IExpert[] _experts;
        protected IBlackboard _blackboard;
        protected IScoreboard<ScoreboardEntry> _scoreboard;
        private IArbiter arbiter;

        /// <summary>
        /// list of experts that should be call
        /// </summary>
        public IExpert[] obligatories
        {
            get { return _obligatories; }
        }

        /// <summary>
        /// moderator marks experts based on their entries
        /// </summary>
        public IModerator moderator
        {
            get { return _moderator; }
        }
        
        /// <summary>
        /// list of experts that could be call if there's already not enough experts
        /// 
        /// Maximum number of experts is defined in the config file by attribute "MaxExpertsNumber"
        /// Minimum number of experts is defined in the config file by attribute "MinExpertsNumber"
        /// </summary>
        public IExpert[] competitives
        {
            get { return _competitives; }
        }

        /// <summary>
        /// list of unique experts
        /// </summary>
        override public IExpert[] experts
        {
            get { return _experts; }
        }

        /// <summary>
        /// board with entries <see cref="BlackboardEntry"/>
        /// </summary>
        override public IBlackboard blackboard
        {
            get { return _blackboard; }
        }
        
        /// <summary>
        /// board with the scores <see cref=" ScoreboardEntry"/>
        /// </summary>
        public IScoreboard<ScoreboardEntry> scoreboard
        {
            get { return _scoreboard; }
        }
        #endregion

        protected override Logger getLogger()
        {
            return l;
        }

        private void InitVariable()
        {
            this.stages = PG_Scoreboard.Utils.Configuration.stages;
        }

        /// <summary>
        /// creates Scoreboard controller with given parameters.
        /// Gets only unique experts from given list,
        /// gets last arbiter from arbiters table,
        /// gets last moderator from moderators array
        /// and init stages given in configuration file as values of parameter "Stages".
        /// The value are names of the stages separated with semicolon, for example: "Stage1;Stage2"
        /// </summary>
        /// <param name="blackboard">blackboard object</param>
        /// <param name="scoreboard">scoreboard object</param>
        /// <param name="experts">array of experts</param>
        /// <param name="moderator">array of moderators</param>
        /// <param name="arbiter">array of arbiters</param>
        /// <param name="obligatory">array of obligatory experts</param>
        /// <param name="competitives">array of optional experts</param>
        [Inject]
        public ScoreboardController(IBlackboard blackboard,
                                    IScoreboard<ScoreboardEntry> scoreboard,
                                    IExpert[] experts,
                                    IModerator[] moderator,
                                    IArbiter[] arbiter,
            [Obligatory] IExpert[] obligatory,
            [Competitive]IExpert[] competitives)
        {
            this._blackboard = blackboard;
            this._scoreboard = scoreboard;

            this._experts = GetUnique(experts);
            this.arbiter = arbiter.Last();
            this._moderator = moderator.Last();
            this._obligatories = obligatory;
            this._competitives = competitives;

            InitVariable();
        }

        
        /// <summary>
        /// Launches standard controller loop, finishes upon finding solution, or when
        /// none of the experts have anything to add.
        /// </summary>
        override public void run()
        {
            l.log.Debug("-- Starting Scoreboard controller loop");

            if (stages == null || stages.Count < 1)
            {
                l.log.Fatal("ERROR: no stages set");
                return;
            }

            bool nothingAdded;

            foreach (String stage in stages)
            {
                l.log.Debug("---- next stage - " + stage);
                
                do
                {
                    nothingAdded = true;
                    l.log.Debug("Current turn: " + blackboard.GetCurrentTurn() + " Stage: " + stage);

                    foreach (var expert in arbiter.GetActiveExperts(stage))
                    {
                        tryLaunchExpert(expert, ref nothingAdded);
                    }
			        blackboard.ChangeTurn();   

                    if (moderator.processesPhase(stage))
                        moderator.DoWork();
                }
                while (!nothingAdded && blackboard.GetCurrentTurn() < PG_Scoreboard.Utils.Configuration.MaxRoundInPhaseNumber);

                blackboard.ChangePhase();
            }

            l.log.Debug("---- Scores");
            foreach (ScoreboardEntry e in scoreboard.GetEntries())
            {
                l.log.Debug(e.ScoredName + ": " + e.Score);
            }
        }
    }
}
