﻿/* Copyright (c) 2015, pg-scoreboard
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * User: Macr
 * Date: 2014-03-17
 * Time: 21:04
 */
using System;
using System.Collections.Generic;
using System.Linq;

using Ninject;
using PG_Scoreboard.BlackboardImpl;
using PG_Scoreboard.Interfaces;
using PG_Scoreboard.Interfaces.IBlackboardParameters.Base;
using PG_Scoreboard.NinjectConfig;
using PG_Scoreboard.Utils;
using PG_Scoreboard.Interfaces.Base;

namespace PG_Scoreboard.Controllers
{
    /// <summary>
    /// BlackboardController is an implementation of a workflow in standard Blackboard
    /// pattern.
    /// </summary>
    public class BlackboardController : AbstractController
    {
        private void InitVariable()
        {
            this.stages = PG_Scoreboard.Utils.Configuration.stages;
        }

        protected override Logger getLogger()
        {
            return l;
        }

        protected IExpert[] _experts;
        protected IBlackboard _blackboard;

        /// <summary>
        /// Function to get blackboard from controller
        /// </summary>
        /// <returns>Blackboard object</returns>
        override public IBlackboard blackboard
        {
            get
            {
                return _blackboard;
            }
        }
        /// <summary>
        /// Function to get list of experts
        /// </summary>
        /// <returns>Array of experts from controller</returns>
        override public IExpert[] experts
        {
            get { return _experts; }
        }

        /// <summary>
        /// Constructor of blackboad controller.
        /// * Gets unique experts from list of experts and save them in memory.
        /// * Gets given blackboard and save it in the memory.
        /// * init stages given in configuration file as values of parameter "Stages".
        /// The value are names of the stages separated with semicolon, for example: "Stage1;Stage2"
        /// </summary>
        /// <param name="blackboard">Blackboard by which controller should be initiated</param>
        /// <param name="experts">List of experts by whome controller should be initiated</param>
        [Inject]
        public BlackboardController(IBlackboard blackboard, IExpert[] experts)
        {
            this._blackboard = blackboard;
            this._experts = GetUnique(experts);
            InitVariable();
        }

        /// <summary>
        /// Launches standard controller loop, finishes upon finding solution, or when
        /// none of the experts have anything to add.
        /// </summary>
        override public void run()
        {
            l.log.Debug("-- Starting Blackboard controller loop");
            if (stages == null || stages.Count < 1)
            {
                l.log.Fatal("ERROR: no stages set");
                return;
            }

            bool nothingAdded = true;

            foreach (String stage in stages)
            {
                l.log.Debug("---- next stage - " + stage);
                do
                {
                    nothingAdded = true;
                    l.log.Debug("Current turn: " + blackboard.GetCurrentTurn());
                    /* debug
                    BlackboardEntry blEntr = blackboard.GetEntries().FirstOrDefault();
                    string entryVal = blEntr != null ? (blEntr.data.ToString()) : "[]";

                    l.log.Debug("Data (only first shown): " + entryVal);
                    */

                    foreach (var expert in experts.Where(x => x.processesPhase(stage)))
                    {
                        tryLaunchExpert(expert, ref nothingAdded);
                    }
                    blackboard.ChangeTurn();
                }
                while (!nothingAdded);
                blackboard.ChangePhase();
            }
            l.log.Debug("Job finished, nothing more to add, end");
        }
    }
}
