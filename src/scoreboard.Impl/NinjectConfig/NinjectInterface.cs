﻿/* Copyright (c) 2015, pg-scoreboard
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * User: Macr
 * Date: 2014-03-17
 * Time: 20:06
 */
using System;
using System.Linq;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using Ninject;
using Ninject.Modules;
using Ninject.Infrastructure;
using Ninject.Planning.Bindings;
using System.Reflection;
using Ninject.Extensions.Xml;
using PG_Scoreboard.Interfaces;


namespace PG_Scoreboard.NinjectConfig
{
	/// <summary>
	/// NinjectInterface is an internal singleton class, that exposes 
	/// Dependency Injection fnctionality
	/// </summary>
	public class NinjectInterface
	{
        private static IKernel kernel;

        private static void StartNinject()
        {
            
            kernel = new StandardKernel(new NinjectModule[] { new Module() });
    		kernel.Settings.AllowNullInjection = true;

            string config = PG_Scoreboard.Utils.Configuration.ConfigFile;
            if(!string.IsNullOrWhiteSpace(config))
                LoadFromConfigFile(config);
        }

        static NinjectInterface()
        {
            StartNinject();
        }
        
        /// <summary>
        /// Resolves object for given interface
        /// </summary>
        /// <typeparam name="T">
        /// Interface for which we need to resolve instance
        /// </typeparam>
        /// <returns>
        /// Instance implementing given interface, specified in binding file
        /// </returns>
		public static T resolve<T>()
		{
			return ((IEnumerable<T>)kernel.GetAll<T>()).Last();
		}

        public static T resolve<T>(string name)
        {
            return kernel.Get<T>(name);
        }

        // used in controller/scoreboard etc override
        public static IEnumerable<T> resolveAll<T>()
        {            
            return kernel.GetAll<T>();
        }

        /// <summary>
        /// Disposes default kernel, and exchanges it for one with custom modules 
		/// used for mocking out for tests.
        /// </summary>
        /// <remarks>
        /// Removes all previously loaded modules
        /// </remarks>
        /// <param name="modules">modules that should replace existing configuration</param>
		public static void mockOutModules(NinjectModule[] modules)
		{
			kernel.Dispose();
			kernel = new StandardKernel(modules);
			kernel.Settings.AllowNullInjection = true;
		}
		
		/// <summary>
		/// Alternative for LoadFromConfigFile, allows for creating of declarative bindings
		/// within code.
		/// </summary>
		/// <param name="module">Valid module class, implementing NinjectModule</param>
		public static void LoadModuleAsClass(NinjectModule module)
		{
            
			kernel.Load(module);
		}

        /// <summary>
        /// Function to get bindings from kernel Xml module
        /// </summary>
        /// <returns>list of bindings</returns>
        public static List<IBinding> GetBindings()
        {
            
            var module = kernel.GetModules().OfType<XmlModule>().Single();
            var t2 = module.Bind<IExpert>();
                var bindings = module.Bindings.
                ToList();
                return bindings;
        }
        
		/// <summary>
		/// clears kernel, necessary due to singleton objects.
		/// </summary>
		public static void ClearKernel()
		{
			kernel.Dispose();
            StartNinject();

		}

		/// <summary>
		/// Load kernel from file
		/// </summary>
		/// <param name="path">path to the file</param>
        public static void LoadFromConfigFile(string path)
        {
            if (File.Exists(path))
                kernel.Load(path);
        }
	}
}
