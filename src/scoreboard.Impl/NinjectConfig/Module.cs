﻿/* Copyright (c) 2015, pg-scoreboard
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * User: Macr
 * Date: 2014-03-17
 * Time: 20:01
 */
using System;
using System.Configuration;
using Ninject;
using Ninject.Modules;
using PG_Scoreboard.BlackboardImpl;
using PG_Scoreboard.Controllers;
using PG_Scoreboard.Interfaces;
using PG_Scoreboard.ScoreboardImpl;

namespace PG_Scoreboard.NinjectConfig
{
	/// <summary>
	/// module for configuring ninject
	/// </summary>
	public class Module : NinjectModule
	{
        /// <summary>
        /// Function to load necessary objects:
        /// blackboard controller, blackoard and scoreboard - the last two as singletones
        /// </summary>
		public override void Load()
		{
            this.Bind<IController>().To<BlackboardController>();
            
            this.Bind<IBlackboard>().To<Blackboard>().InSingletonScope();
            this.Bind<Blackboard>().ToSelf().InSingletonScope();
            
            this.Bind<IScoreboard<ScoreboardEntry>>().To<Scoreboard>().InSingletonScope();
            this.Bind<Scoreboard>().ToSelf().InSingletonScope();
		}
	}
}
