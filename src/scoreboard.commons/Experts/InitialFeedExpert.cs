﻿/* Copyright (c) 2015, pg-scoreboard
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * User: Macr
 * Date: 2014-04-07
 * Time: 20:10
 */
using System;
using System.Collections.Generic;
using PG_Scoreboard.BlackboardImpl;
using PG_Scoreboard.Interfaces;
using PG_Scoreboard.Interfaces.IExpertParameters;
using PG_Scoreboard.Interfaces.IExpertParameters.Base;
using scoreboard.commons.Data;
using scoreboard.Commons.Experts.Base;

namespace scoreboard.Commons.Experts
{
	/// <summary>
	/// Description of InitialFeedExpert.
	/// </summary>
	public class InitialFeedExpert : BaseExpertExample
	{
		private int counter = 0;
		
		public InitialFeedExpert(IBlackboard blk)
			: base(blk)
		{
			
		}
		
		public override int InitCondition(List<BlackboardEntry> entries)
		{
			if (counter < 1)
			{
				counter++;
				return 0;
			}
			return 1;
		}
		
		public override void DoWork()
		{
			blackboard.AddEntry(new BlackboardEntry(new DataNode
                {
                	dta = "_INIT_"
                }, GetName(), Types.INIT, 1));
		}
		
		public override string GetName()
		{
			 return "InitialFeed";
		}
	}
}
