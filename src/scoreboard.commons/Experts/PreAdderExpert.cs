﻿/* Copyright (c) 2015, pg-scoreboard
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * User: Macr
 * Date: 2014-04-07
 * Time: 20:11
 */
using System;
using System.Collections.Generic;
using System.Linq;

using PG_Scoreboard.BlackboardImpl;
using PG_Scoreboard.Interfaces;
using PG_Scoreboard.Interfaces.IBlackboardParameters.Base;
using PG_Scoreboard.Interfaces.IExpertParameters;
using PG_Scoreboard.Interfaces.IExpertParameters.Base;
using scoreboard.commons.Data;
using scoreboard.Commons.Experts.Base;

namespace scoreboard.Commons.Experts
{
	/// <summary>
	/// Description of PreAdderExpert.
	/// </summary>
	public class PreAdderExpert : BaseExpertExample
	{
		public PreAdderExpert(IBlackboard blk)
			: base(blk)
		{
			
		}
		
		public override int InitCondition(List<BlackboardEntry> entries)
		{
			var available = entries.Where(x =>
			                              x.Type.CompareTo(Types.PARTIAL) == 0 ||
			                              x.Type.CompareTo(Types.EVAL) == 0);
			return available.Count() > 0 ? 0 : 1;
		}

        public override void DoWork()
		{
			string[] types = new string[] { Types.PARTIAL, Types.EVAL };
			
			BlackboardEntry singleEntry = null;
			
			foreach (string type in types)
			{
				singleEntry = blackboard
					.GetEntries(new SimpleBlackboardEntryType(type)).FirstOrDefault();
				if (singleEntry != null)
					break;
			}
			
			if (singleEntry != null)
			{
				DataNode data = singleEntry.data as DataNode;
				
				if (singleEntry.Type.CompareTo(Types.EVAL) == 0)
					singleEntry.Type = new SimpleBlackboardEntryType(Types.PARTIAL);
				
				if (data.preNeed > 0)
				{
					data.dta = data.dta.Insert(0, "PREPEND_");
					data.preNeed--;
				}
			}
		}
		
		public override string GetName()
		{
			 return "PreAdder";
		}
	}
}
