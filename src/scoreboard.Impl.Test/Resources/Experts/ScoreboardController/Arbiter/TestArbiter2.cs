﻿/*
 * User: Macr
 * Date: 2014-06-02
 * Time: 03:26
 */
using System;
using System.Collections.Generic;
using System.Linq;

using PG_Scoreboard.Interfaces;
using PG_Scoreboard.Interfaces.IBlackboardParameters.Base;
using PG_Scoreboard.ScoreboardImpl;
using PG_Scoreboard.ScoreboardImpl.Arbiter;

namespace PG_Scoreboard.Test.Resources.Experts.ScoreboardController.Arbiter
{
	/// <summary>
	/// Description of TestArbiter.
	/// </summary>
	public class TestArbiter : BaseArbiter
	{
		private IExpert[] experts;
		private IScoreboard<ScoreboardEntry, ScoreboardEntryScore> scoreboard;
		
		public TestArbiter(IExpert[] experts, IScoreboard<ScoreboardEntry, ScoreboardEntryScore> scoreboard)
			: base(scoreboard, experts)
		{
			this.scoreboard = scoreboard;
			this.experts = experts;
		}
		
		public override List<IExpert> GetActiveExperts()
		{
			return GetActiveExperts(this.experts);
		}
		
		public List<IExpert> GetActiveExperts(IExpert[] experts)
		{
			List<ScoreboardEntry> entries = scoreboard.GetEntries();
			
			List<IExpert> toActivate = new List<IExpert>();
			
			foreach (IExpert expert in experts)
			{
				var scoreEntry = entries.Find(x => x.ScoredName == expert.GetName());
				
				if (scoreEntry == null || 
				    (((FloatScoreboardEntryScore)scoreEntry.score).CompareTo(0.6f) >= 0))
					toActivate.Add(expert);
			}
			
			return toActivate;
		}
		
		public override List<IExpert> GetActiveExperts(SimpleBlackboardEntryType type)
		{
			IExpert[] experts = this.experts.Where(x => x.processesPhase(type)).ToArray();
			
			return GetActiveExperts(experts);
		}
	}
}
