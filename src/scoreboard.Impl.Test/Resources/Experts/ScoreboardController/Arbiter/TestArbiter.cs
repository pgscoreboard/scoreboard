﻿/* Copyright (c) 2015, pg-scoreboard
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * User: Macr
 * Date: 2014-06-02
 * Time: 03:26
 */
using System;
using System.Collections.Generic;
using System.Linq;

using PG_Scoreboard.Interfaces;
using PG_Scoreboard.Interfaces.IBlackboardParameters.Base;
using PG_Scoreboard.ScoreboardImpl;
using PG_Scoreboard.ScoreboardImpl.Arbiter;

namespace PG_Scoreboard.Test.Resources.Experts.ScoreboardController.Arbiter
{
	/// <summary>
	/// Description of TestArbiter.
	/// </summary>
	public class TestArbiter : BaseArbiter
	{
		private IExpert[] experts;
        private IScoreboard<ScoreboardEntry> scoreboard;

        public TestArbiter(IExpert[] experts, IScoreboard<ScoreboardEntry> scoreboard, IExpert[] obligatory, IExpert[] competitives)
			: base(scoreboard, experts,obligatory,competitives)
		{
			this.scoreboard = scoreboard;
			this.experts = experts;
		}
		
		public override List<IExpert> GetActiveExperts()
		{
			return GetActiveExperts(this.experts);
		}

        public override List<IExpert> GetActiveExperts(string type, int max,int min)
        {
            if (experts.Count() < max)
                return GetActiveExperts(this.experts);
            return GetActiveExperts((this.experts).Take(max).ToArray());
        }

		public List<IExpert> GetActiveExperts(IExpert[] experts)
		{
			List<ScoreboardEntry> entries = scoreboard.GetEntries();
			
			List<IExpert> toActivate = new List<IExpert>();
			
			foreach (IExpert expert in experts)
			{
				var scoreEntry = entries.Find(x => x.ScoredName == expert.GetName());
				
				if (scoreEntry == null || 
				    ((scoreEntry.Score).CompareTo(0.6f) >= 0))
					toActivate.Add(expert);
			}
			
			return toActivate;
		}
		
		public override List<IExpert> GetActiveExperts(string type)
		{
			IExpert[] experts = this.experts.Where(x => x.processesPhase(type)).ToArray();
			
			return GetActiveExperts(experts);
		}
	}
}
