﻿/* Copyright (c) 2015, pg-scoreboard
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * User: Macr
 * Date: 2014-06-02
 * Time: 03:16
 */
using System;
using System.Collections.Generic;
using System.Linq;

using PG_Scoreboard.BlackboardImpl;
using PG_Scoreboard.Interfaces;
using PG_Scoreboard.Interfaces.Base;
using PG_Scoreboard.Interfaces.IBlackboardParameters.Base;
using PG_Scoreboard.Interfaces.IExpertParameters.Base;

namespace PG_Scoreboard.Test.Resources.Experts.ScoreboardController
{
	public abstract class BaseTestExpert : BaseExpert
	{
		protected abstract string PHASE { get; }
		protected abstract string PRODUCES_FOR { get; }
		
		protected List<BlackboardEntry> alreadyProcessed;
		
		public BaseTestExpert(IBlackboard blackboard) : base (blackboard)
		{
			condition = new BaseWorkCondition(
				(entries) =>
		        {
					var usableEntries = entries.Where(x => x.Type.CompareTo(PHASE) == 0);
					usableEntries = usableEntries.Where(x => x.ModuleName != this.GetName());
					
					return usableEntries.Except(alreadyProcessed).Count() > 0 ? 0 : 1;
				});
			
			alreadyProcessed = new List<BlackboardEntry>();
		}
		
		protected virtual BlackboardEntry GetDataFromBlackboard()
		{
			var entries = this
				.blackboard
				.GetEntries(GetName(), new SimpleBlackboardEntryType(PHASE))
				.Except(alreadyProcessed);
			
			var entry = entries.First();
			alreadyProcessed.Add(entry);
			
			return entry;
		}
	}
}
