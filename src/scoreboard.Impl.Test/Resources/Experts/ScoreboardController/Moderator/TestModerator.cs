﻿/* Copyright (c) 2015, pg-scoreboard
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * User: Macr
 * Date: 2014-06-02
 * Time: 03:41
 */
using System;
using System.Collections.Generic;
using System.Linq;

using PG_Scoreboard.BlackboardImpl;
using PG_Scoreboard.Interfaces;
using PG_Scoreboard.Interfaces.IBlackboardParameters;
using PG_Scoreboard.Interfaces.IBlackboardParameters.Base;
using PG_Scoreboard.ScoreboardImpl;

namespace PG_Scoreboard.Test.Resources.Experts.ScoreboardController.Moderator
{
	/// <summary>
	/// Description of TestModerator.
	/// </summary>
	public class TestModerator : BaseTestExpert, IModerator
	{
		IScoreboard<ScoreboardEntry> scoreboard;
		
		public TestModerator(IBlackboard blackboard, 
		                     IScoreboard<ScoreboardEntry> scoreboard) 
			: base(blackboard)
		{
			this.scoreboard = scoreboard;
		}
		
		protected override string PRODUCES_FOR {
			get 
			{
				return "NAN";
			}
		}
		
		protected override string PHASE {
			get 
			{
				return "*";
			}
		}
		
		protected new IEnumerable<BlackboardEntry> GetDataFromBlackboard()
		{
			var entries = this
				.blackboard
				.GetEntries(GetName(), new SimpleBlackboardEntryType(PHASE))
				.Except(alreadyProcessed)
				.ToList();
			
			alreadyProcessed.AddRange(entries);
			
			return entries;
		}

        public override void DoWork()
		{
			var entries = GetDataFromBlackboard();
			double score = 0;

			foreach (var entry in entries)
			{
				string data = entry.data as string;
				string creator = entry.ModuleName;
				
				Action<string> setScoreFunc = (contains) =>
				{
                    if (data.Contains(contains))
                        score = 1;
                    else
                        score = 0;
					
					ScoreboardEntry scoreEntry = new ScoreboardEntry(creator, score);
                    scoreboard.AddEntry(scoreEntry);
				};
				
				switch (entry.Type.type)
				{
					case "INIT":
					{
                        ScoreboardEntry scoreEntry = new ScoreboardEntry(creator, 1.0f);
                        scoreboard.AddEntry(scoreEntry);
					}
					break;
						
					case "PROCESS":
					{
						setScoreFunc("_INIT");
					}
					break;
					
					case "EVAL":
					{
						setScoreFunc("_PROCESS");
					}
					break;
					
				default:
					break;
				}
			}
		}
		
		public override string GetName()
		{
			return "ModeratorExpert";
		}
		
		public override bool processesPhase(string phase)
		{
			return true;
		}
	}
}
