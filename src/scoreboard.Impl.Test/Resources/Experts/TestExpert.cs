﻿/* Copyright (c) 2015, pg-scoreboard
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * User: Macr
 * Date: 2014-03-17
 * Time: 19:35
 */
using System;
using Ninject;
using PG_Scoreboard.Interfaces;
using PG_Scoreboard.Interfaces.IExpertParameters;
using PG_Scoreboard.Utils;
using PG_Scoreboard.Interfaces.Base;

namespace PG_Scoreboard.Test.Resources.Experts
{
	/// <summary>
	/// Description of TestExpert.
	/// </summary>
	public class TestExpert : BaseExpert
	{
		private Logger log = new Logger(typeof(TestExpert));

        public TestExpert()
            : base(null)
        {

        }

        override public void DoWork()
		{
			log.log.Debug("Doing work");
		}

        override public string GetName()
        {
            return this.ToString();
        }

        override public IWorkCondition GetWorkCondition()
		{
			throw new NotImplementedException();
		}

        override public bool processesPhase(string phase)
		{
			throw new NotImplementedException();
		}
	}
}
