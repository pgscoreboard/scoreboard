﻿/* Copyright (c) 2015, pg-scoreboard
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * User: Macr
 * Date: 2014-06-02
 * Time: 02:22
 */
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Ninject.Modules;
using NUnit.Framework;
using PG_Scoreboard.BlackboardImpl;
using PG_Scoreboard.Controllers;
using PG_Scoreboard.Interfaces;
using PG_Scoreboard.Interfaces.IBlackboardParameters.Base;
using PG_Scoreboard.NinjectConfig;
using PG_Scoreboard.ScoreboardImpl;
using PG_Scoreboard.Test.Resources.MockModules.Chain;
using PG_Scoreboard.Test.Resources.MockModules.ScoreboardControllerMocks;

namespace PG_Scoreboard.Test.Controllers
{
    /// <summary>
    /// Description of ScoreboardControllerTest.
    /// </summary>
    [TestFixture]
    public class ScoreboardControllerTest
    {
        [SetUp]
        public void Init()
        {

            //PG_Scoreboard.Utils.Configuration.InitValue();
            try
            {
                if (File.Exists(PG_Scoreboard.Utils.Configuration.BlackboardPath))
                    File.Delete(PG_Scoreboard.Utils.Configuration.BlackboardPath);
                if (File.Exists(PG_Scoreboard.Utils.Configuration.ScoreboardPath))
                    File.Delete(PG_Scoreboard.Utils.Configuration.ScoreboardPath);
            }
            catch
            { }
            PG_Scoreboard.Utils.Configuration.MaxRoundInPhaseNumber = 1;
            NinjectInterface.mockOutModules(
                new NinjectModule[] { new ScoreboardControllerTestModule() });
        }

        /// <summary>
        /// flow of process: INIT(inserted into blackboard as initial data -> LANGSPEC -> TEXTFIX -> EVAL
        /// Test receives 2 pieces of data as init, as of ScoreboardControllerTestModule there are 3 processExpert, so there will be 5 
        /// (there is a bad process, that will get 0 on first turn) results in EVAL phase
        /// </summary>
        [Test]
        public void run_ControllerProperlyRunsEachExpert_Returns5ProcessEntries()
        {
            PG_Scoreboard.Utils.Configuration.stages = new List<string>() 
                                {
                                    "INIT",
                                    "PROCESS"
                                };

            IController controller = NinjectInterface.resolve<IController>();

            controller.blackboard.AddEntry(new BlackboardEntry("Testing", ".", "INIT", 1.0));
            controller.blackboard.AddEntry(new BlackboardEntry("AnotherStarter", ".", "INIT", 1.0));

         
            controller.run();

            var results = controller.blackboard.GetEntries(new SimpleBlackboardEntryType("EVAL"));
            //5 = 4 experts + moderator
            Assert.AreEqual(5, results.Count);
        }

        /// <summary>
        /// flow of process: INIT(inserted into blackboard as initial data -> LANGSPEC -> TEXTFIX -> EVAL
        /// Test receives 2 pieces of data as init, as of ScoreboardControllerTestModule there are 4 experts (initExpert and 3 processExpert)
        /// </summary>
        [Test]
        public void run_ModeratorProperlyScores_Returns4Scores()
        {
            PG_Scoreboard.Utils.Configuration.stages = new List<string>() {
                "INIT","PROCESS"
            };
            IController controller = NinjectInterface.resolve<IController>();

            controller.blackboard.AddEntry(new BlackboardEntry("Testing", "Name1", "INIT", 1.0));
            controller.blackboard.AddEntry(new BlackboardEntry("AnotherStarter", "Name2", "INIT", 1.0));
            
            controller.run();

            var results = ((ScoreboardController)controller).scoreboard.GetEntries();
            var score = results.Where(x => x.ScoredName == "BadProcessExpert");

            Assert.NotNull(score.First());
            Assert.AreEqual(0.0, score.First().Score);
            Assert.Greater(results.Count, 4);
        }
    }
}
