﻿/* Copyright (c) 2015, pg-scoreboard
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * User: Macr
 * Date: 2014-03-17
 * Time: 20:53
 */
using System;
using Ninject.Modules;
using NUnit.Framework;
using PG_Scoreboard.Interfaces;
using PG_Scoreboard.NinjectConfig;
using PG_Scoreboard.Test.Resources.Controllers;
using PG_Scoreboard.Test.Resources.MockModules;

namespace PG_Scoreboard.Test.Ninject
{
	[TestFixture]
	public class ControllerInjectTest
	{
		[TestFixtureSetUp]
		public void Init()
		{
			NinjectInterface.mockOutModules(new NinjectModule[] { new MultiMockModule() });
		}
		
		/* should be 2 TestExpert, 1 Test2Expert and 1 Test3Expert */
		[Test]
		public void controllerConstructor_canInjectMultipleExperts_ReturnsMoreThan1()
		{
			IController controller = null;
			
			controller = NinjectInterface.resolve<IController>();
			
			Assert.AreEqual(((TestController)controller).experts.Length, 4);
		}
	}
}
