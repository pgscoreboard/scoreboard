﻿/* Copyright (c) 2015, pg-scoreboard
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * User: Macr
 * Date: 2014-06-01
 * Time: 21:37
 */
using System;
using Ninject;
using NUnit.Framework;
using PG_Scoreboard.NinjectConfig;
using PG_Scoreboard.ScoreboardImpl;

namespace PG_Scoreboard.Test.ScoreboardImpl
{
	/// <summary>
	/// Description of ScoreboardTest.
	/// </summary>
	[TestFixture]
	public class ScoreboardTest
	{
        private IKernel ninjectKernel;

        [TestFixtureSetUp]
        public void setup()
        {
            ninjectKernel = new StandardKernel(new Module());
        }

        [Test]
        public void Ninject_Create2Scoreboards_AreSame()
        {
            Scoreboard scoreboardFirst = ninjectKernel.Get<Scoreboard>();
            Scoreboard scoreboardSecond = ninjectKernel.Get<Scoreboard>();

            Assert.AreEqual(scoreboardFirst, scoreboardSecond);
        }

        //Not always, if entry for that Module exists there will be no new entry, only update
      /*  [Test]
        public void AddEntry_AddsEntry_ScoreoardHasOneMoreEntry()
        {
            Scoreboard scoreboard = ninjectKernel.Get<Scoreboard>();
            ScoreboardEntry entry = new ScoreboardEntry("DefaultModule", 1.0f);

            int firstContentSize = scoreboard.GetEntries().Count;
            scoreboard.AddEntry(entry);
            int secondContentSize = scoreboard.GetEntries().Count;

            Assert.AreEqual(firstContentSize + 1, secondContentSize);
        }*/
	}
}
