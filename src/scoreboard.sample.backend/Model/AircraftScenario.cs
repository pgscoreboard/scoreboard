﻿/* Copyright (c) 2015, pg-scoreboard
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
using Ninject.Modules;
using PG_Scoreboard.Controllers;
using PG_Scoreboard.Interfaces;
using PG_Scoreboard.NinjectConfig;
using scoreboard.aircraftAnalyzer.Constants;
using scoreboard.aircraftAnalyzer.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PG_Scoreboard.Interfaces.IBlackboardParameters.Base;

namespace scoreboard.sample.backend.Model
{
    public class AircraftScenario : Scenario
    {
        public bool ForTest { get; set; }

        public override NinjectModule ModuleForScenario { get; set; }

        public override string Name { get; set; }

        public AircraftScenario()
        {
            Name = typeof(AircraftScenario).Name;
            ModuleForScenario = new AircraftModule();
        }

        private List<AircraftModuleTest.Context> testExperts()
        {
            List<AircraftModuleTest.Context> list = new List<AircraftModuleTest.Context>();

            list.Add(new AircraftModuleTest.Context()
            {
                inType = null,
                outType = EntryTypes.RADAR_SIGNATURE,
                name = "scoreboard.scenario.aircraftAnalyzer.test.SurveyorExpert",
                phase = Phases.RADAR_PHASE,
                successRate = 0.9,
                timeMod = 1.0
            });

            list.Add(new AircraftModuleTest.Context()
                {
                    inType = EntryTypes.RADAR_SIGNATURE,
                    outType = EntryTypes.GUESS_PARTS,
                    name = "scoreboard.scenario.aircraftAnalyzer.test.AltitudeExpert",
                    phase = Phases.PROCESS_PHASE,
                    successRate = 0.9,
                    timeMod = 1.0
                });
            list.Add(new AircraftModuleTest.Context()
            {
                inType = EntryTypes.RADAR_SIGNATURE,
                outType = EntryTypes.GUESS_PARTS,
                name = "scoreboard.scenario.aircraftAnalyzer.test.SpeedExpert",
                phase = Phases.PROCESS_PHASE,
                successRate = 0.9,
                timeMod = 1.0
            });
            list.Add(new AircraftModuleTest.Context()
            {
                inType = EntryTypes.RADAR_SIGNATURE,
                outType = EntryTypes.GUESS_PARTS,
                name = "scoreboard.scenario.aircraftAnalyzer.test.SizeExpert",
                phase = Phases.PROCESS_PHASE,
                successRate = 0.9,
                timeMod = 1.0
            });

            list.Add(new AircraftModuleTest.Context()
            {
                inType = EntryTypes.GUESS_PARTS,
                outType = EntryTypes.RESULTS,
                name = "scoreboard.scenario.aircraftAnalyzer.test.GuessExpert",
                phase = Phases.GUESS_PHASE,
                successRate = 0.9,
                timeMod = 1.0
            });

            return list;
        }

        public override string UseScoreboardScenario(string configFile)
        {
            ForTest = true;

            NinjectInterface.ClearKernel();

            if (ForTest)
            {
                AircraftModuleTest module = new AircraftModuleTest();
                module.preload(testExperts());

                NinjectInterface.LoadModuleAsClass(module);
            }
            else
            {
                NinjectInterface.LoadModuleAsClass(this.ModuleForScenario);
            }

            PG_Scoreboard.Utils.Configuration.stages = new List<string>() 
                                {
                Phases.RADAR_PHASE_T,
                Phases.PROCESS_PHASE_T,
                Phases.GUESS_PHASE_T
                                };
			IController controller = NinjectInterface.resolve<ScoreboardController>();

            controller.run();

            if (ForTest)
            {
                scoreboard.aircraftAnalyzer.Tests.TestModerator mod = (controller as ScoreboardController).moderator
                    as scoreboard.aircraftAnalyzer.Tests.TestModerator;

                StringBuilder builder = new StringBuilder();

                foreach (string key in mod.Results.Keys)
                {
                    builder
                        .Append(key + ":\n")
                        .Append("\tPASSED: ").Append(mod.Results[key].Passed)
                        .Append("\tFAILED: ").Append(mod.Results[key].Failed);
                }

                return builder.ToString();
            }
            else
            {
                scoreboard.aircraftAnalyzer.SpecialExperts.Moderator mod = (controller as ScoreboardController).moderator
                as scoreboard.aircraftAnalyzer.SpecialExperts.Moderator;

                StringBuilder builder = new StringBuilder();

                foreach (string key in mod.Results.Keys)
                {
                    builder
                        .Append(key + ":\n")
                        .Append("\tPASSED: ").Append(mod.Results[key].Passed)
                        .Append("\tFAILED: ").Append(mod.Results[key].Failed);
                }

                return builder.ToString();
            }
        }

        public override string UseBlackboardScenario(string configFile)
        {
            NinjectInterface.ClearKernel();
            NinjectInterface.LoadModuleAsClass(this.ModuleForScenario);
            PG_Scoreboard.Utils.Configuration.stages = new List<string>() 
                                {
                Phases.RADAR_PHASE_T,
                Phases.PROCESS_PHASE_T,
                Phases.GUESS_PHASE_T
                                };
            IController controller = NinjectInterface.resolve<BlackboardController>();

            controller.run();

            return "";
        }
    }
}
