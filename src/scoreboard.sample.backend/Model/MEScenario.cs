﻿/* Copyright (c) 2015, pg-scoreboard
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
using Ninject.Modules;
using PG_Scoreboard.Interfaces;
using PG_Scoreboard.Interfaces.IBlackboardParameters.Base;
using PG_Scoreboard.NinjectConfig;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PG_Scoreboard.BlackboardImpl;
using Monitor_emocji;
using PG_Scoreboard.ScoreboardImpl;

namespace scoreboard.sample.backend.Model
{
    public class MEScenario : Scenario
    {
        public override NinjectModule ModuleForScenario { get; set; }

        public override string Name { get; set; }

        public MEScenario()
        {
            Name = "Monitor emocji";
        }

        /* various inits */
        public override void Before()
        {
            PG_Scoreboard.Utils.ProcessScript.SetStages(new List<string>() { "PROCESS" });
            
        }
        public override void After()
        {

        }

        public override string UseScoreboardScenario(string configFile)
        {
            if (configFile == "")
                return "Wybierz plik konfiguracyjny dla Monitora Emocji";
            configFile = configFile.Replace("Blackboard", "Scoreboard");
            string result = "";
            Before();
            
            var controller = PG_Scoreboard.Utils.ProcessScript.RunScoreboard("..\\..\\Resources\\"+configFile);
            After();
            var ModeratorResult = controller.blackboard.GetEntries().Where(x => x.ModuleName == "ModeratorExpert");
            var Results = controller.scoreboard.GetEntries();
            

            foreach (BlackboardEntry entry in ModeratorResult)
                result += ";;;;"+((PADstate)entry.data).ShortDisplay();// +entry.ModuleName;
            //foreach (ScoreboardEntry entry in Results)
            //    result += entry.ScoredName + entry.CallNumber+"\n";

            return result;
        }

        public override string UseBlackboardScenario(string configFile)
        {
            if (configFile == "")
                return "Wybierz plik konfiguracyjny dla Monitora Emocji";
            configFile = configFile.Replace("Scoreboard", "Blackboard");
            
            string result = "";
            Before();
            PG_Scoreboard.Utils.ProcessScript.SetStages(new List<string>() { "PROCESS" ,"SUMARIZE" });
            var controller = PG_Scoreboard.Utils.ProcessScript.RunBlackboard("..\\..\\Resources\\"+configFile);
            After();
            var ModeratorResult = controller.blackboard.GetEntries().Where(x => x.ModuleName == "ModeratorExpert");


            foreach (BlackboardEntry entry in ModeratorResult)
                result += ((PADstate)entry.data).ShortDisplay();// +entry.ModuleName;

            return result;

        }
    }
}
