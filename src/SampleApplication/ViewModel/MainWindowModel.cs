﻿/* Copyright (c) 2015, pg-scoreboard
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
using PG_Scoreboard.Interfaces;
using PG_Scoreboard.NinjectConfig;
using scoreboard.sample.backend.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;

namespace scoreboard.sample.backend.ViewModel
{
    public class MainWindowModel : INotifyPropertyChanged
    {
        private IScenario _CurrentScenario;
        public IScenario CurrentScenario
        {
            get { return _CurrentScenario; }
            set { _CurrentScenario = value; NotifyPropertyChanged("CurrentScenario"); }
        }
        private string _CurrentConfigFile;
        public string CurrentConfigFile
        {
            get { return _CurrentConfigFile; }
            set { _CurrentConfigFile = value; NotifyPropertyChanged("CurrentConfigFile"); }
        }

        public ObservableCollection<IScenario> Scenarios { get; set; }
        public ObservableCollection<string> ConfigFiles { get; set; }

        public MainWindowModel()
        {
            Scenarios = new ObservableCollection<IScenario>();
            ConfigFiles = new ObservableCollection<string>();
            String sourceDirectory = ConfigurationManager.AppSettings["ConfigDirectory"];
            var txtFiles = Directory.EnumerateFiles(sourceDirectory, "*.xml");

            foreach (string currentFile in txtFiles)
            {
                string fileName = currentFile.Substring(sourceDirectory.Length + 1);
                ConfigFiles.Add(fileName);
            }
        }

        /* necessary, since we should not initialize items before bindings are done */
        public void Init()
        {
            Scenarios.Add(new AircraftScenario());
            Scenarios.Add(new MEScenario());

        }

        #region useless_INotifyPropertyChanged_boilerplate

        public event PropertyChangedEventHandler PropertyChanged;
        
        private void NotifyPropertyChanged(String propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (null != handler)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion
    }
}
