﻿/* Copyright (c) 2015, pg-scoreboard
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
using PG_Scoreboard.BlackboardImpl;
using PG_Scoreboard.Controllers;
using PG_Scoreboard.Interfaces;
using PG_Scoreboard.Interfaces.IBlackboardParameters.Base;
using PG_Scoreboard.NinjectConfig;
using PG_Scoreboard.ScoreboardImpl;
using PG_Scoreboard.Utils;
using scoreboard.commons.Module;
using scoreboard.sample.backend.Model;
using scoreboard.sample.backend.ViewModel;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Diagnostics;
using System.IO;

namespace SampleApplication
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindowModel model { get; set; }

        public MainWindow()
        {
            model = new MainWindowModel();

            InitializeComponent();
            
            /* set data context globally to viewModel */
            this.DataContext = model;

            model.Init();
            loadParameters();
        }

        private void loadParameters()
        {
            //PG_Scoreboard.Utils.Configuration.InitValue();
            MaxPhaseNumber.Text = Configuration.MaxPhasesInTheMemory.ToString();
            MaxTurnsNumber.Text = Configuration.MaxTurnsInTheMemory.ToString();
            MaxExpertNumber.Text = Configuration.MaxExpertsNumber.ToString();
            MinExpertNumber.Text = Configuration.MinExpertsNumber.ToString();

            RefreshScore();
        }

        public void Start_Activate(object sender, RoutedEventArgs e)
        {

            if (ScoreboardCheckBox.IsChecked == false && BlackboardCheckBox.IsChecked == false)
            {
                MessageBox.Show("You must check at least one pattern!");
                return;
            }
            else if (model.CurrentScenario == null)
            {
                MessageBox.Show("You must check any scenario!");
                return;
            }

            int run = 1;
            int.TryParse(TextBoxRunCount.Text,out run);
            //bool 
            string result = "";
            Stopwatch stopWatch = new Stopwatch();
            
            while(run>0)
            {
            stopWatch.Restart();

            try
            {
                if (CheckBoxClearScoreboard.IsChecked == true && File.Exists(PG_Scoreboard.Utils.Configuration.ScoreboardPath))
                    File.Delete(PG_Scoreboard.Utils.Configuration.ScoreboardPath);
            }
            catch { }

            if (BlackboardCheckBox.IsChecked == true )
            {
                string tmp = "";
                stopWatch.Start();
                tmp = model.CurrentScenario.UseBlackboardScenario(ListBoxConfigFiles.SelectedValue.ToString());
                stopWatch.Stop();
                if (CheckBoxDescription.IsChecked == true)
                {
                    if (ChecBoxTime.IsChecked == true)
                        result += stopWatch.ElapsedMilliseconds + ";";
                }
                else
                    if (ChecBoxTime.IsChecked == true)
                        result += "Czas działania blackboard: " + stopWatch.ElapsedMilliseconds + "\n" + tmp + "\n";
                    else result += tmp;
            }

            if (ScoreboardCheckBox.IsChecked == true)
            {
                
                string tmp = "";
                stopWatch.Restart();
                tmp = model.CurrentScenario.UseScoreboardScenario(ListBoxConfigFiles.SelectedValue.ToString());
                stopWatch.Stop();
                if (CheckBoxDescription.IsChecked == true)
                {
                    if (ChecBoxTime.IsChecked == true)
                        result += stopWatch.ElapsedMilliseconds + ";";
                }
                else
                    if (ChecBoxTime.IsChecked == true)
                        result += "Czas działania scoreboard: " + stopWatch.ElapsedMilliseconds + "\n" + tmp + "\n";
                    else result += tmp;
            
                
                
                
                if (CheckBoxScore.IsChecked == true)
                {
                    RefreshScore();
                    result += GetScore();
                }
            }
            result += "\n";
            run--;
            }
            RefreshScore();
            if(CheckBoxFileBool.IsChecked==true)
                System.IO.File.AppendAllText(@".\"+ListBoxConfigFiles.SelectedValue.ToString()+".csv", result);                
            else MessageBox.Show(result);
 
        }

        private void RefreshScore()
        {
            NinjectInterface.ClearKernel();
            Scoreboard scoreboard = NinjectInterface.resolve<Scoreboard>();
            List<ScoreboardEntry> list = scoreboard.GetEntries();
            TableOfExperts.ItemsSource = list;
        }

        private string GetScore()
        {

            string result = "";
            foreach (ScoreboardEntry entry in TableOfExperts.ItemsSource)
                result += ";;;;;;;;;"+entry.Score + ";"+ entry.ScoredName +";"+ entry.CallNumber+ "\n";
            return result;
            //MessageBox.Show(result);
        
        }
        public void Scenarios_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            model.CurrentScenario = e.AddedItems.Count > 0 ? (IScenario)e.AddedItems[0] : null;
        }

        public void ConfigFiles_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            model.CurrentConfigFile = e.AddedItems.Count > 0 ? (string)e.AddedItems[0] : null;
        }
        

        private void MaxRoundNumber_TextChanged_1(object sender, TextChangedEventArgs e)
        {
            int tmp;
            if (int.TryParse(MaxTurnsNumber.Text, out tmp))
                PG_Scoreboard.Utils.Configuration.MaxTurnsInTheMemory = tmp;
        }

        private void MaxPhaseNumber_TextChanged_1(object sender, TextChangedEventArgs e)
        {
            int tmp;
            if (int.TryParse(MaxPhaseNumber.Text, out tmp))
                PG_Scoreboard.Utils.Configuration.MaxPhasesInTheMemory = tmp;
        }

        private void MinExpertNumber_TextChanged(object sender, TextChangedEventArgs e)
        {
            int tmp;

            if (int.TryParse(MinExpertNumber.Text, out tmp))
                PG_Scoreboard.Utils.Configuration.MinExpertsNumber = tmp;
        }

        private void MaxExpertNumber_TextChanged(object sender, TextChangedEventArgs e)
        {
            int tmp;

            if (int.TryParse(MaxExpertNumber.Text, out tmp))
                PG_Scoreboard.Utils.Configuration.MaxExpertsNumber = tmp;
        }

        
    }
}
